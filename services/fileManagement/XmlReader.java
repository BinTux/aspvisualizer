/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services.fileManagement;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This is an XML reader.
 * @author Andrea
 */
public class XmlReader {
	
	private Element rootElement;
	
	public XmlReader(String filePath){
		try{
			File xmlFile = new File(filePath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document parsedDocument = dBuilder.parse(xmlFile);
			parsedDocument.getDocumentElement().normalize();
			rootElement = parsedDocument.getDocumentElement();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public XmlReader (Element root){
		this.rootElement = root;
	}
	
	/*
	 * Returns a XMLReader list, one for each node.
	 */	
	public Collection<XmlReader> getXMLReaderList(String id){
		Collection<XmlReader> readers = new ArrayList<>();
		NodeList nodes = rootElement.getElementsByTagName(id);
		for (int i = 0; i < nodes.getLength(); i++){
			readers.add(new XmlReader((Element)nodes.item(i)));
		}
		return readers;
	}
	
	public XmlReader getXMLReader (String id){
		return new XmlReader((Element)rootElement.getElementsByTagName(id).item(0));
		//TODO provare
	}
	
	/*
	 * Returns the string content of a node.
	 */
	public String getStringContent (String id){
		return rootElement.getElementsByTagName(id).item(0).getTextContent();
	}
	
	/*
	 * Returns the integer content of a node.
	 */
	public int getIntContent (String id){
		return Integer.parseInt(rootElement.getElementsByTagName(id).item(0).getTextContent());
	}
	
	/**
	 * Retreives a boolean from the specified id.
	 * @param id
	 * @return 
	 */
	public boolean getBooleanContent (String id){
		return Boolean.parseBoolean(rootElement.getElementsByTagName(id).item(0).getTextContent());
	}
	
	/*
	 * Returns a node list containing id as tag. 
	 */
	public NodeList getNodeList (String id){
		return rootElement.getElementsByTagName(id);
	}
	
	/*
	 * Returns all the childern of a node as node list.
	 */
	public NodeList getChildren (Node node){
		return node.getChildNodes();
	}
	
	public Element getRoot(){
		return rootElement;
	}
	
}
