package services.fileManagement;

import bl.aspDomain.Predicate;
import bl.representationDomain.GridAxis;
import bl.representationDomain.GridGraphicDefinition;
import java.util.List;
import java.util.Set;


public class GridDefinitionDirector {
	
	private static GridDefinitionDirector instance = null;	
	private IGraphicDefinitionBuilder gridBuilder;
	
	public static GridDefinitionDirector getInstance() {
		if(instance == null) {
			instance = new GridDefinitionDirector();
		}
		return instance;
	}	
	
	public void initGridDefinitionDirector(IGraphicDefinitionBuilder gridBuilder) {
		this.gridBuilder = gridBuilder;		
	}
	
	public void buildGridGraphicDefinitionConfiguration(GridGraphicDefinition gridDefinition) {
		gridBuilder.addOpenChild("grid","");
		if(gridDefinition.getBackground()!=null) {
			setBackground(gridDefinition.getBackground());
		}
		setOverlapping(gridDefinition.isAttributeOverlapping());
		setDomainAndOrder(GridAxis.ROW, gridDefinition);
		setDomainAndOrder(GridAxis.COLUMN, gridDefinition);
		setPredicates(gridDefinition);
		gridBuilder.moveToParent();		
	}
	private void setBackground(String background) {
		gridBuilder.addCloseChild("background", background);
	}

	private void setOverlapping(boolean attributeOverlapping) {
		gridBuilder.addCloseChild("overlapping", Boolean.toString(attributeOverlapping));
	}

	private void setDomainAndOrder(GridAxis gridAxis, GridGraphicDefinition gridDefinition) {
		gridBuilder.addOpenChild("domainAndOrder", "");
		gridBuilder.addCloseChild("axis", gridAxis.toString().toLowerCase());
		gridBuilder.addOpenChild("domain", "");
		boolean userDomain = gridDefinition.isUserDomainSet(gridAxis);
		gridBuilder.addCloseChild("manuallySpecified", Boolean.toString(userDomain));
		setDomainValues(gridAxis,gridDefinition,userDomain);
		gridBuilder.moveToParent();
		gridBuilder.addCloseChild("order", gridDefinition.getDomainOrder(gridAxis).toString().toLowerCase());
		gridBuilder.moveToParent();
	}

	private void setDomainValues(GridAxis gridAxis, GridGraphicDefinition gridDefinition, boolean userDomain) {
		if(userDomain) {
			List<String> values = gridDefinition.getUserDomain(gridAxis);
			String domainString = "";
			for(int i=0;i<values.size();i++) {
				domainString=domainString.concat(values.get(i));
				if(i<values.size()-1) {
					domainString=domainString.concat(",");
				}
			}
			gridBuilder.addCloseChild("values", domainString);
		}
		else {
			String domainString= "";
			Set<Predicate> predicates = gridDefinition.getAnswerSetDomains(gridAxis).keySet();
			Predicate [] predicatesArray = new Predicate[predicates.size()];
			predicates.toArray(predicatesArray);
			for(int i=0;i<predicates.size();i++) {
				List<Integer> indexes= gridDefinition.getAnswerSetDomains(gridAxis).get(predicatesArray[i]);
				for(int j=0;j<indexes.size();j++) {
					domainString = domainString.concat(predicatesArray[i].getName()+":"+indexes.get(j));
					if(j<indexes.size()-1) {
						domainString = domainString.concat(",");
					}						
				}
				if(i<predicates.size()-1) {
					domainString = domainString.concat(",");
				}
			}
			gridBuilder.addCloseChild("values", domainString);
		}
	}

	private void setPredicates(GridGraphicDefinition gridDefinition) {
		gridBuilder.addOpenChild("predicates", "");
		for(Predicate predicate: gridDefinition.getIndexingAttributes(GridAxis.ROW).keySet()) {
			gridBuilder.addOpenChild("predicate", "");
			gridBuilder.addCloseChild("name", predicate.getName());
			gridBuilder.addOpenChild("indexingAttributes", "");
			gridBuilder.addCloseChild(GridAxis.ROW.toString().toLowerCase(), gridDefinition.getIndexingAttributes(GridAxis.ROW).get(predicate).toString());
			gridBuilder.addCloseChild(GridAxis.COLUMN.toString().toLowerCase(), gridDefinition.getIndexingAttributes(GridAxis.COLUMN).get(predicate).toString());
			gridBuilder.moveToParent();
			gridBuilder.addCloseChild("fieldIdentifier", Integer.toString(gridDefinition.getVisualDescriptions().get(predicate).getFieldIdentifier()));
			gridBuilder.addCloseChild("imageAssociation", Boolean.toString(gridDefinition.getVisualDescriptions().get(predicate).isAnImageAssociated()));
			gridBuilder.moveToParent();
		}
		gridBuilder.moveToParent();
	}
}
