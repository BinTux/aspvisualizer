package services.fileManagement;

import java.io.File;

public interface IGraphicDefinitionBuilder {
		
	public void addOpenChild(String childName, String content);
	
	public void moveToParent();
	
	public void addCloseChild(String childName,String content);
	
	public void writeProduct(File file);
	
	public void inizializeProduct();
}
