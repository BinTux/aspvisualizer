package services.fileManagement;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlBuilder implements IGraphicDefinitionBuilder { 

	private Document doc;
	private Element currentElement;
	private static XmlBuilder instance;
	
	public static XmlBuilder getInstance() {
		if(instance == null) {
			instance = new XmlBuilder();
		}
		return instance;
	}

	private XmlBuilder() {
	}	
	
	@Override
	public final void inizializeProduct() {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			currentElement = doc.createElement("visualization");
			doc.appendChild(currentElement);
			}catch (ParserConfigurationException pce) {
				pce.printStackTrace();
		 }
	}

	@Override
	public void addOpenChild(String childName, String content) {
		Element childElement = doc.createElement(childName);
		childElement.appendChild(doc.createTextNode(content));
		currentElement.appendChild(childElement);
		currentElement = childElement;
	}

	@Override
	public void moveToParent() {
		currentElement = (Element) currentElement.getParentNode();
	}

	@Override
	public void addCloseChild(String childName, String content) {
		Element childElement = doc.createElement(childName);
		childElement.appendChild(doc.createTextNode(content));
		currentElement.appendChild(childElement);
	}
	
	@Override
	public void writeProduct(File file) {
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result);
			
		} catch (TransformerException ex) {
			ex.printStackTrace();
		} 
	}
	
}
