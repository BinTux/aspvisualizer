
package services.fileManagement;

import bl.aspDomain.Predicate;
import bl.representationDomain.TableGraphicDefinition;
import java.util.List;
import java.util.Set;


public class TableDefinitionDirector {
	
	private IGraphicDefinitionBuilder tableBuilder;
	private static TableDefinitionDirector instance = null;
	
	public static TableDefinitionDirector getInstance() {
		if(instance == null) {
			instance = new TableDefinitionDirector();
		}
		return instance;
	}
	
	public void initTableDefinitionDirector(IGraphicDefinitionBuilder builder) {
		this.tableBuilder = builder;
	}
	
	public void buildTableGraphicDefinitionConfiguration(TableGraphicDefinition tableDefinition) {
		tableBuilder.addOpenChild("table","");
		setJoin(tableDefinition);
		setPredicates(tableDefinition);
		tableBuilder.moveToParent();
	}

	private void setJoin(TableGraphicDefinition tableDefinition) {
		tableBuilder.addCloseChild("aJoinIsSet", Boolean.toString(tableDefinition.hasJoinBeenSet()));
		if(tableDefinition.hasJoinBeenSet()) {
			tableBuilder.addOpenChild("join", "");
			tableBuilder.addCloseChild("name", tableDefinition.getJoin().getName());
			List<String> joinAttributes = tableDefinition.getJoin().getJoinAttributes();
			String attributeString = "";
			for(int i=0; i <joinAttributes.size();i++) {
				attributeString = attributeString.concat(joinAttributes.get(i));
				if(i< joinAttributes.size()-1) {
					attributeString = attributeString.concat(",");
				}
			}
			tableBuilder.addCloseChild("attributes",attributeString);
			tableBuilder.addCloseChild("mergeColumns",Boolean.toString(tableDefinition.getJoin().areColumnsMerged()));
			tableBuilder.moveToParent();
			
		}
	}

	private void setPredicates(TableGraphicDefinition tableDefinition) {
		tableBuilder.addOpenChild("predicates", "");
		
		Set<Predicate> predicatesSet = tableDefinition.getAttributeLabels().keySet();
		for(Predicate predicate: predicatesSet) {
			tableBuilder.addOpenChild("predicate", "");
			tableBuilder.addCloseChild("name", predicate.getName());
			tableBuilder.addOpenChild("labels", "");
			List<String> labelValues = tableDefinition.getAttributeLabels() .get(predicate);
			int labelCounter = 1;
			for(String value : labelValues) {
				tableBuilder.addCloseChild("index", Integer.toString(labelCounter));
				labelCounter++;
				tableBuilder.addCloseChild("value", value);
						
			}
			tableBuilder.moveToParent();
			tableBuilder.moveToParent();
			
		}
		tableBuilder.moveToParent();
	}

}
