/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package services.dlpParsing;

import bl.aspDomain.AnswerSet;
import bl.aspDomain.Atom;
import bl.aspDomain.Predicate;
import it.unical.mat.dlv.parser.ParseException;
import it.unical.mat.dlv.program.Literal;
import it.unical.mat.dlv.program.Term;
import it.unical.mat.wrapper.Model;
import it.unical.mat.wrapper.exception.NoModelParseException;
import it.unical.mat.wrapper.util.parseModel.ParserModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The parser class is a tool used for creating an answer set in the asp visualizer application.
 * @author Andrea
 */
public class Parser {
	
	private static ParserModel parserModel;
	private static Parser parser;
	private static AnswerSet answerSet;
	
	private Parser (){
		parserModel = new ParserModel();
	}
	
	public static Parser getInstance (){
		if (parser == null){
			parser = new Parser();
		}
		return parser;
	}
	/**
	 * Giving a file containing a model, this method performs a parsing of the file and creates
	 * the application answer set object.
	 * @param file The file containing the user's answer set.
	 * @throws FileNotFoundException Thrown if the file doesn't exists.
	 * @throws ParseException Thrown if the model isn't correct. 
	 * @throws NoModelParseException 
	 */
	//TODO NoModelParseException cos'è?
	public void parse (File file) throws FileNotFoundException, ParseException, NoModelParseException  {
		createAnswerSet(parserModel.parse(file));
	}
	
	/**
	 * Giving an input stream connected to a model, this method performs a parsing of the stream and creates
	 * the application answer set object.
	 * @param istream The stream connected with an answer set.
	 * @throws ParseException Thrown if the model isn't correct.
	 * @throws NoModelParseException 
	 */
	public void parse (InputStream istream) throws ParseException, NoModelParseException{
		createAnswerSet(parserModel.parse(istream));
	}
	
	/**
	 * This method creates an answer set basing on the model passed.
	 * @param model The model to examinate and to trasform to an application answer set.
	 */
	private void createAnswerSet(Model model){
		Predicate tempPredicate;
		Atom tempAtom = null;
		List<String> attributeList = new ArrayList<>();
		Map<Predicate,List<Atom>> atoms = new HashMap<>();
		while(model.hasMorePredicates()){
			it.unical.mat.wrapper.Predicate parsedPredicate = model.nextPredicate();
			tempPredicate = new Predicate(parsedPredicate.arity(),parsedPredicate.name());
			Enumeration<Literal> literals =  parsedPredicate.getLiterals();
			while (literals.hasMoreElements()){
				Literal parsedLiteral = literals.nextElement();
				List<Term> parsedAttributeList = parsedLiteral.attributes();
				attributeList = new ArrayList<>();
				for (Term currentTerm : parsedAttributeList){
					attributeList.add(currentTerm.toString());
				}
				tempAtom = new Atom(tempPredicate, attributeList);
				if(tempAtom != null){
					if(atoms.get(tempPredicate) == null){
						List <Atom> newList = new ArrayList<>();
						newList.add(tempAtom);
						atoms.put(tempPredicate,newList);
					}else{
						atoms.get(tempPredicate).add(tempAtom);
					}
				}
			}
		}
		answerSet = new AnswerSet(atoms);
	}
	
	public AnswerSet getAnswerSet(){
		return answerSet;
	}
	
}
