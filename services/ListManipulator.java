package services;

import com.sun.java.swing.plaf.windows.WindowsTreeUI;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class ListManipulator {
	
	public static List<String> getAlphaOrderedList(List<String> stringList) {
		List<String> resultList = new LinkedList<>();
		resultList.addAll(stringList);
		Collections.sort(resultList);
		return resultList;
	}
	
	public static List<String> getInverseAlphaOrderedList(List<String> stringList) {
		List<String> resultList = getAlphaOrderedList(stringList);		
		Collections.reverse(resultList);
		return resultList;
	}
	
}	
