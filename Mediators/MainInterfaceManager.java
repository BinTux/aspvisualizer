
package Mediators;

import GUI.gridGraphicInterface.GridDefinitionPanel;
import GUI.gridGraphicInterface.GridDialog;
import GUI.mainInterface.MainFrame;
import GUI.services.FileChooser;
import GUI.tableInterface.TableDialog;
import Mediators.TableInterfaceManager;
import bl.aspDomain.AnswerSet;
import bl.aspDomain.Predicate;
import bl.representationDomain.BlManager;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.JDialog;


public class MainInterfaceManager {
	private MainFrame frame;
	private static MainInterfaceManager instance;
	
	
	public static MainInterfaceManager getInstance() {
		if(instance == null) {
			instance = new MainInterfaceManager();
		}
		return instance;
	}

	public MainFrame getFrame() {
		return frame;
	}
	
	public void applicationLaunched() {
		frame = new MainFrame("AspVisualizer");
		frame.createAndShow();
		
	}
	
	public void submitAnswerSetRequest() {
		FileChooser.getInstance().chooseDlpFile();
	}

	public void submitChoosenFile(File file) {
		BlManager.getInstance().submitAnswerSet(file);
		
	}

	public void answerSetSubmitted(AnswerSet answerSet) {
		
		List<String> columns = new LinkedList<>();
		int maxAriety = answerSet.getMaxAriety();
		columns.add("Predicate name");
		for(int i=1;i<=maxAriety;i++) {
			columns.add("attribute label "+i);
		}
		frame.getPredicatesPanel().addColumns(columns);
		for(Predicate p: answerSet.getPredicates()) {
			frame.getPredicatesPanel().addRow(p.getPredicateNameAndAttributesLabels());
		}
		frame.getPredicatesPanelAndOptions().activateButtons();
	}

	public void updateSelectedPredicates(Set<String> selectedRowFirstValues) {
		if(selectedRowFirstValues.isEmpty()) {
			frame.getGraphicRepresentationPanel().disactivatePanel();
		}
		else {
			frame.getGraphicRepresentationPanel().activatePanel();
			boolean gridCanBeActivated = true;
			for(String predicate: selectedRowFirstValues) {
				int ariety = BlManager.getInstance().getAnswerSet().getPredicateMap().get(predicate).getAriety();
				if(ariety<2) {
					gridCanBeActivated = false;
				}
			}
			if(!gridCanBeActivated) {
				frame.getGraphicRepresentationPanel().disactivateGrid();
			}
		}
	}

	public void closeEditAnswerSetDialog() {
		frame.getEditAnswerSetDialog().dispose();
	}

	public void createTableDefinition() {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
	public void addTableGraphicDefinition(){
		TableInterfaceManager.getInstance().tableSelected(frame, convertNamesToPredicates());
	}
	public void addGridGraphicDefinition(){
		new GridDialog(convertNamesToPredicates());
	}
	
	private Set<Predicate> convertNamesToPredicates (){
		Set<String> predicatesName = frame.getPredicatesPanel().getSelectedRowFirstValues();
		Set<Predicate> predicates = new HashSet<>();
		for (String currentName : predicatesName){
			predicates.add(BlManager.getInstance().getPredicateFromName(currentName));
		}
		return predicates;
	}
	
}
