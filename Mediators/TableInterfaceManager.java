
package Mediators;

import GUI.tableInterface.TableDialog;
import bl.aspDomain.Predicate;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.JFrame;


public class TableInterfaceManager {
	
	private static TableInterfaceManager instance;
	private TableDialog table;

	private TableInterfaceManager() {
	}
	
	public void tableSelected(JFrame frame, Set<Predicate> selectedPredicates) {
		table = new TableDialog(frame);		
		table.init(getNamesFromPredicates(selectedPredicates));
	}
	
	public static TableInterfaceManager getInstance() {
		if(instance == null) {
			instance = new TableInterfaceManager();
		}
		return instance;
	}
	
	public void joinActivated(boolean checked) {
		table.changeJoinStatus(checked);		
	}

	private List<String> getNamesFromPredicates(Set<Predicate> selectedPredicates) {
		List<String> names = new LinkedList<>();
		for(Predicate p:selectedPredicates) {
			names.add(p.getName());
		}
		return names;
	}
	
	
}
