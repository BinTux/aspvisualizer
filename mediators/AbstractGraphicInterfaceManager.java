/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mediators;

import GUI.sharedComponents.ChangeLabelsDialog;
import GUI.sharedComponents.GraphicDefinitionDialog;
import bl.aspDomain.AnswerSet;
import bl.aspDomain.Atom;
import bl.aspDomain.Predicate;
import bl.appState.BlManager;
import bl.representationDomain.GraphicDefinition;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Andrea
 */
public abstract class AbstractGraphicInterfaceManager implements GraphicInterfaceManager {
	
	protected ChangeLabelsDialog changeLabelsDialog;
	private GraphicDefinitionDialog dialog;
	protected GraphicDefinition definition;
			
	public AbstractGraphicInterfaceManager(){}
	
	public void graphicDefinitionSelected(GraphicDefinitionDialog dialog,GraphicDefinition definition){
		this.dialog = dialog;
		this.definition = definition;
	}
	
	@Override
	public void closeEditLabelsDialog() {
		changeLabelsDialog.dispose();
	}
	
	/*
	 * creates a dialog used to change labels over the selected predicate on the proper list
	 */
	@Override
	public void changeLabelsDialogRequest() {
		String selectedPredicateString = (String) dialog.getPredicateList().getCurrentSelection();
		AnswerSet answerSet = BlManager.getInstance().getAnswerSet();
		Predicate selectedPredicate = answerSet.getPredicateMap().get(selectedPredicateString);
		List<String> labels = definition.getPredicateLabels(selectedPredicate);
		changeLabelsDialog = new ChangeLabelsDialog(dialog.getOwnerFrame(),this);
		changeLabelsDialog.init(labels, selectedPredicateString);
	}
	
	@Override
	public void saveModifiedLabelsOnDefinition() {
		String predicateString = changeLabelsDialog.getModifingPredicate();
		TableModel tableModel = changeLabelsDialog.getTable().getModel();
		List<String> newLabels = new LinkedList<>();
		for(int i=0; i<tableModel.getColumnCount();i++) {
			newLabels.add((String) tableModel.getValueAt(0, i));
		}
		Predicate predicate = BlManager.getInstance().getPredicateFromName(predicateString);
		definition.getAttributeLabels().put(predicate,newLabels);
		dialog.getAtomsTable().setColumnLabels(newLabels.toArray(new String[0]));
		
	}
	
	@Override
	public void labelChanged(int row, int column) {
		TableModel model = changeLabelsDialog.getTable().getModel();
		String changedLabel = (String )model.getValueAt(row, column);
		String changedPredicate = changeLabelsDialog.getModifingPredicate();
		if(!changedLabel.matches("[a-zA-Z0-9 ]+") || definition.labelIsDuplicated(changedLabel,column,changedPredicate)) {
			String oldLabel = definition.getPredicateLabels(changedPredicate).get(column);
			model.setValueAt(oldLabel, row, column);
		}
		else {				
			definition.setPredicateAttributeLabelTo(changedPredicate,column, changedLabel);
		}
	}
	
	
	/*
	 * Retrieves a string list containing all predicates name in the parameter set.
	 */
	protected List<String> getNamesFromPredicates(Set<Predicate> selectedPredicates) {
		List<String> names = new LinkedList<>();
		for(Predicate p:selectedPredicates) {
			names.add(p.getName());
		}
		return names;
	}
	
	/*
	 * Updates labels on predicate table basing on the current selection
	 */

	public void updateSelectedPredicates() {
		String selectedPredicateString = (String) dialog.getPredicateList().getCurrentSelection();
		Predicate selectedPredicate = BlManager.getInstance().getPredicateFromName(selectedPredicateString);
		List<String> labels = definition.getPredicateLabels(selectedPredicate);
		List<Atom> atoms = BlManager.getInstance().getAtomsFromPredicate(selectedPredicate);
		DefaultTableModel predicatesTableModel = new DefaultTableModel(labels.toArray(), 0);
		for(Atom atom : atoms) {
			predicatesTableModel.addRow(atom.getAttributes().toArray());
		}
		dialog.getAtomsTable().setModel(predicatesTableModel);
	}	
	

	protected void overWriteLabels() {
		BlManager.getInstance().overWriteLabels(this.definition.getAttributeLabels());
	}
	
	public GraphicDefinition getDefinition(){
		return definition;
	}
	
	public GraphicDefinitionDialog getDialog(){
		return dialog;
	}
	
}