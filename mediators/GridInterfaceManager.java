/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mediators;

import GUI.gridGraphicInterface.DomainPane;
import GUI.gridGraphicInterface.GridDefinitionPanel;
import GUI.gridGraphicInterface.GridDialog;
import listeners.PredicateListener;
import bl.aspDomain.Predicate;
import bl.appState.BlManager;
import bl.representationDomain.GridAxis;
import bl.representationDomain.GridGraphicDefinition;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.JFrame;
import listeners.LabelsListener;

/**
 *
 * @author Andrea
 */
public class GridInterfaceManager extends AbstractGraphicInterfaceManager{
	
	private List<PredicateListener> predicateListeners= new ArrayList<>();
	private List<LabelsListener> labelsListeners = new ArrayList<>();
	private Set<Predicate> involvedPredicates;
	private GridDialog dialog;
	
	public GridInterfaceManager (JFrame frame,Set<Predicate> involvedPredicates,GridGraphicDefinition definition){
		this.involvedPredicates = involvedPredicates;
        this.dialog = new GridDialog(frame,new GridDefinitionPanel());
		super.graphicDefinitionSelected(dialog,definition);
		initGUI();
	}
	
	public void selectionChanged (String newSelectedPredicate){
		for (PredicateListener currentListener : predicateListeners){
			currentListener.predicateChanged(newSelectedPredicate);
		}
	}
	
	public String getCurrentSelectedPredicate(){
		return dialog.getGridPanel().getPredicateList().getCurrentSelection();
	}
	
	public List<String> getInvolvedPredicatesNames(){
		return getNamesFromPredicates(involvedPredicates);
	}
	
	public void registerToPredicateListeners (PredicateListener listener){
		predicateListeners.add(listener);
	}
	
	public void registerToLabelsListeners (LabelsListener listener){
		labelsListeners.add(listener);
	}
	
	public Set<Predicate> getInvolvedPredicates (){
		return involvedPredicates;
	}
	
	public void disposeDialog(){
		dialog.dispose();
	}
	
	public void submitGraphicDefinition(){
		if(dialog.getGridPanel().getOthersPane().willLabelBeOverwritten()){
			super.overWriteLabels();
		}
		BlManager.getInstance().getCurrentGraphicDefinition().setName("Grid definition "+ BlManager.getInstance().getGridDefinitionNumber());
		BlManager.getInstance().addGraphicDefinition();
		MainInterfaceManager.getInstance().definitionCompleted();
	}

	private void initGUI(){
		dialog.initDialog(this);
	}
	
	public void labelsChanged (String [] newLabels){
		System.out.println(labelsListeners.size());
		for (LabelsListener currentListener : labelsListeners){
			currentListener.labelsChanged(newLabels);
		}
	}

	@Override
	public GridGraphicDefinition getDefinition () {
		return (GridGraphicDefinition)definition;
	}
	
	public DomainPane getDomainPane (GridAxis axis){
		return dialog.getGridPanel().getDomainPane().getSpecificDomainPane(axis);
	}
	
	public void domainChanged(GridAxis axis){
		dialog.getGridPanel().getDomainPane().getSpecificDomainPane(axis).changeDomains();
	}
	
	public void indexingAttributeChanged(GridAxis type, Integer itemIndex) {
		((GridGraphicDefinition)definition).setIndexingAttribute(type,getCurrentSelectedPredicate(),itemIndex);
	}
	
	public void visualDescriptionChanged (Integer itemIndex){
		getDefinition().setVisualDescription(getCurrentSelectedPredicate(), itemIndex);
	}
	
	public int getVisualDescriptor (){
		return getDefinition().getVisualDescriptions().get(BlManager.getInstance().getPredicateFromName(getCurrentSelectedPredicate())).getFieldIdentifier();
	}
	
}
