/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mediators;

import java.util.List;

/**
 *
 * @author Andrea
 */
public interface GraphicInterfaceManager {
	
	void changeLabelsDialogRequest();
	void closeEditLabelsDialog();
    void saveModifiedLabelsOnDefinition();
	void labelChanged(int row, int column);
}
