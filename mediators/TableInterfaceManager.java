
package mediators;

import GUI.sharedComponents.ChangeLabelsDialog;
import GUI.tableInterface.JoinPanel;
import GUI.tableInterface.TableDialog;
import bl.aspDomain.AnswerSet;
import bl.aspDomain.Atom;
import bl.aspDomain.Predicate;
import bl.appState.BlManager;
import bl.representationDomain.Join;
import bl.representationDomain.TableGraphicDefinition;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


public class TableInterfaceManager extends AbstractGraphicInterfaceManager{
	
	private static TableInterfaceManager instance;
	private TableDialog tableDialog;
	private JFrame mainFrame;
	private TableGraphicDefinition tableDefinition;
	private int instantiatedTables = 0;

	private TableInterfaceManager() {}
	/*
	 * It inizialize a new table dialog basing on the selected predicates.
	 */
	public void tableSelected(JFrame frame, TableGraphicDefinition definition) {
		tableDialog = new TableDialog(frame);	
		tableDefinition = definition;
		super.graphicDefinitionSelected(tableDialog,definition);
		mainFrame = frame;
		tableDialog.init(getNamesFromPredicates(definition.getAttributeLabels().keySet()));
	}
	
	public static TableInterfaceManager getInstance() {
		if(instance == null) {
			instance = new TableInterfaceManager();
		}
		
		return instance;
	}
	
	/*
	 * Changes the interface status whenever the join checkBox changes
	 */
	public void joinActivated(boolean checked) {
		tableDialog.changeJoinStatus(checked);		
	}
	
	public void submitDefinition() {
		if(joinCheckAndSubmission()) {
			instantiatedTables++;
			BlManager.getInstance().getCurrentGraphicDefinition().setName("Table definition "+ instantiatedTables );
			BlManager.getInstance().addGraphicDefinition();
			if(tableDialog.isOverwriteLabels()) {
				super.overWriteLabels();
			}			
			MainInterfaceManager.getInstance().definitionCompleted();
			tableDialog.dispose();
		}
	}

	public void cancelDefinition() {
		tableDialog.dispose();
	}

	public void addJoinAttributeRequest() {
		String submittingAttribute = tableDialog.getJoinPanel().getAddingField().getText();
		if(submittingAttribute.matches("[a-zA-Z0-9 ]+") && joinAttributeAppearsInAllPredicate(submittingAttribute) && !joinAttributeIsInList(submittingAttribute)) {
			DefaultListModel model = (DefaultListModel) tableDialog.getJoinPanel().getAttributeList().getModel();
			model.addElement(submittingAttribute);
		}
		else {
			JOptionPane.showMessageDialog(mainFrame, "Attribute does not appear in all predicate or has invalid characters or is duplicated.", "Can't add join attribute", JOptionPane.INFORMATION_MESSAGE);
		}
		
		tableDialog.getJoinPanel().getAddingField().setText("");
	}

	public void removeJoinAttributeRequest() {
		DefaultListModel model = (DefaultListModel) tableDialog.getJoinPanel().getAttributeList().getModel();
		model.removeElementAt(tableDialog.getJoinPanel().getAttributeList().getSelectedIndex());
	}
	
	private boolean joinCheckAndSubmission() {
		JoinPanel joinPanel= tableDialog.getJoinPanel();
		if(joinPanel.isJoinSelected()) {
			if(!joinPanel.getJoinName().matches("[a-zA-Z0-9 ]+")) {
				JOptionPane.showMessageDialog(mainFrame, "Join name is not valid.", "Can't submit", JOptionPane.INFORMATION_MESSAGE);
				return false;
			}
			if(joinPanel.getAttributeList().getModel().getSize() == 0) {
				JOptionPane.showMessageDialog(mainFrame, "Join has no join attributes.", "Can't submit", JOptionPane.INFORMATION_MESSAGE);
				return false;
			}			
			boolean mergeColumns = tableDialog.getJoinPanel().isMergeColumn();
			tableDefinition.setJoin(new Join(joinPanel.getJoinName(), getJoinAttributes(), mergeColumns));
		}
		return true;
	}

	private boolean joinAttributeAppearsInAllPredicate(String submittingAttribute) {
		List<String> predicatesStrings = new LinkedList<>();
		ListModel predicateListModel = tableDialog.getPredicateList().getModel();
		int size = predicateListModel.getSize();
		for(int i=0;i<size;i++) {
			predicatesStrings.add((String) predicateListModel.getElementAt(i));
		}
		for(String predicate: predicatesStrings) {
			if(!tableDefinition.getPredicateLabels(predicate).contains(submittingAttribute)) {
				return false;
			}
		}
		return true;
	}

	private boolean joinAttributeIsInList(String submittingAttribute) {
		DefaultListModel model = (DefaultListModel)tableDialog.getJoinPanel().getAttributeList().getModel();
		return model.contains(submittingAttribute);
	}

	private List<String> getJoinAttributes() {
		List<String> joinAttributes = new LinkedList<>();
		DefaultListModel model = (DefaultListModel)tableDialog.getJoinPanel().getAttributeList().getModel();
		for(Object obj: model.toArray()) {
			joinAttributes.add((String) obj);
		}
		return joinAttributes;
	}

}
