
package mediators;

import GUI.mainInterface.MainFrame;
import GUI.services.FileChooser;
import bl.aspDomain.AnswerSet;
import bl.aspDomain.Predicate;
import bl.appState.BlManager;
import bl.representationDomain.GridGraphicDefinition;
import bl.representationDomain.GridGraphicRepresentation;
import bl.representationDomain.TableGraphicDefinition;
import bl.representationDomain.TableGraphicRepresentation;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.DefaultListModel;
import javax.swing.table.TableModel;


public class MainInterfaceManager {
	private MainFrame frame;
	private static MainInterfaceManager instance;
	
	
	public static MainInterfaceManager getInstance() {
		if(instance == null) {
			instance = new MainInterfaceManager();
		}
		return instance;
	}

	public MainFrame getFrame() {
		return frame;
	}
	
	public void applicationLaunched() {
		frame = new MainFrame("AspVisualizer");
		frame.createAndShow();
		
	}
	
	public void submitAnswerSetRequest() {
		FileChooser.getInstance().chooseDlpFile();
	}

	public void submitChoosenFile(File file) {
		BlManager.getInstance().submitAnswerSet(file);
		
	}

	public void answerSetSubmitted(AnswerSet answerSet) {
		
		List<String> columns = new LinkedList<>();
		int maxAriety = answerSet.getMaxAriety();
		columns.add("Predicate name");
		for(int i=1;i<=maxAriety;i++) {
			columns.add("attribute label "+i);
		}
		frame.getPredicatesPanel().addColumns(columns);
		for(Predicate p: answerSet.getPredicates()) {
			frame.getPredicatesPanel().addRow(p.getPredicateNameAndAttributesLabels());
		}
		frame.getPredicatesPanelAndOptions().activateButtons();
		frame.getPredicatesPanelAndOptions().getPredicatesPanel().initLabelEvents();
	}

	public void updateSelectedPredicates(Set<String> selectedRowFirstValues) {
		if(selectedRowFirstValues.isEmpty()) {
			frame.getGraphicRepresentationPanel().disactivatePanel();
		}
		else {
			frame.getGraphicRepresentationPanel().activatePanel();
			boolean gridCanBeActivated = true;
			for(String predicate: selectedRowFirstValues) {
				int ariety = BlManager.getInstance().getAnswerSet().getPredicateMap().get(predicate).getAriety();
				if(ariety<2) {
					gridCanBeActivated = false;
				}
			}
			if(!gridCanBeActivated) {
				frame.getGraphicRepresentationPanel().disactivateGrid();
			}
		}
	}

	public void closeEditAnswerSetDialog() {
		frame.getEditAnswerSetDialog().dispose();
	}

	
	public void addTableGraphicDefinition(){
		BlManager.getInstance().createGraphicDefinition(convertNamesToPredicates(), TableGraphicRepresentation.getInstance());
		TableGraphicDefinition definition = (TableGraphicDefinition) BlManager.getInstance().getCurrentGraphicDefinition();
		TableInterfaceManager.getInstance().tableSelected(frame, definition);
	}
	public void addGridGraphicDefinition(){
        Set<Predicate> predicates = convertNamesToPredicates();
        BlManager.getInstance().createGraphicDefinition(predicates, GridGraphicRepresentation.getInstance());
		new GridInterfaceManager(frame,convertNamesToPredicates(),(GridGraphicDefinition)BlManager.getInstance().getCurrentGraphicDefinition());
	}
	
	private Set<Predicate> convertNamesToPredicates (){
		Set<String> predicatesName = frame.getPredicatesPanel().getSelectedRowFirstValues();
		Set<Predicate> predicates = new HashSet<>();
		for (String currentName : predicatesName){
			predicates.add(BlManager.getInstance().getPredicateFromName(currentName));
		}
		return predicates;
	}

	public void labelChanged(int row, int column) {
		if(column!=0) {
			TableModel model = frame.getPredicatesPanel().getTable().getModel();
			String changedLabel = (String )model.getValueAt(row, column);
			String changedPredicate = (String) model.getValueAt(row,0);
			Predicate predicate = BlManager.getInstance().getPredicateFromName(changedPredicate);
			if(!changedLabel.matches("[a-zA-Z0-9 ]+") || predicate.labelIsDuplicated(changedLabel,column-1)) {
				String oldLabel = predicate.getAttributeLabelAt(column-1);
				model.setValueAt(oldLabel, row, column);
			}
			else {				
				predicate.setAttributeLabelTo(column-1, changedLabel);
			}
		}		
	}	

	public void definitionCompleted() {
		DefaultListModel listModel= (DefaultListModel) frame.getDefinitionsPanelAndOptions().getDefinitionsPanel().getPredicateList().getModel();
		listModel.addElement(BlManager.getInstance().getCurrentGraphicDefinition().getName());
	}

	public void saveToFileRequest() {
		FileChooser.getInstance().chooseSaveFile();
	}

	public void saveToFile(File file) {
		BlManager.getInstance().generateVisualizationFile(file);
	}

	public void exitRequest() {
		this.frame.dispose();
	}
}
