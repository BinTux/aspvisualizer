/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bl.aspDomain;

import java.util.Collections;
import java.util.List;

/**
 * An atom is an instance of a predicate.
 * @author Andrea
 */
public class Atom {
	
	private Predicate predicate;
	private List<String> attributes;
	
	/**
	 * Creates an atom, instance of the specified predicate, having a list of attributes.
	 * @param predicate the predicate which the atom is referred to.
	 * @param attributes atom attributes.
	 */
	public Atom(Predicate predicate, List<String> attributes){
		this.predicate = predicate;
		this.attributes = attributes;
	}
	
	/**
	 * Returns the predicate of this atom. 
	 * @return belonging predicate.
	 */
	public Predicate getPredicate (){
		return predicate;
	}
	
	/**
	 * A List of all the attributes is returned.
	 * @return an unmodifiable list containing all the atom attributes.
	 */
	public List<String> getAttributes(){
		return Collections.unmodifiableList(attributes);
	}
	
	/**
	 * Returns the nth attribute.
	 * @param index specify which attribute has to be chosen.
	 * @return the corrisponding attribute, if the given index exists.
	 */
	public String getAttributeAt (int index){
		return attributes.get(index);
	}
}
