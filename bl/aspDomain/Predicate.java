
package bl.aspDomain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * A predicate is a statement that defines a property. It is composed of an ariety and a name.
 * @author Andrea
 */
public class Predicate {
	
	private String name;
	private int ariety;
	/**
	 * This field represents all the labels associated to each attribute of the predicate.
	 */
	private List<String> attributeDefaultLabels;
	
	/**
	 * Creates a predicate with default attribute labels (A1,A2,[..],An).
	 * @param ariety the number of attributes.
	 * @param name name of the predicate.
	 */
	public Predicate (int ariety, String name){
		this.name = name;
		this.ariety = ariety;
		attributeDefaultLabels = new ArrayList<>();
		for (int i = 0; i < ariety; i++){
			attributeDefaultLabels.add("A"+i);
		}
	}
	
	public String getName(){
		return name;
	}
	
	public int getAriety(){
		return ariety;
	}
	
	/**
	 * Returns all the attribute labels present in a predicate.
	 * @return an unmodifiable list containing all the attribute labels.
	 */
	public List<String> getAttributesLabels(){
		return Collections.unmodifiableList(attributeDefaultLabels);
	}
	
	/**
	 * Returns a specific attribute label.
	 * @param index the position of the interested atom.
	 * @return the atom label, if index exists.
	 */
	public String getAttributeLabelAt (int index){
		return attributeDefaultLabels.get(index);
	}
	
	/**
	 * Set labels for all the attributes in the predicate.
	 * @param attributeLabels labels to be set.
	 */
	public void setAttributesLabels (List<String> attributeLabels){
		this.attributeDefaultLabels = attributeLabels;
	}
	
	/**
	 * Set a label to a specific attribute.
	 * @param index position of the interested attribute.
	 * @param newLabel the new label
	 */
	public void setAttributeLabelTo (int index,String newLabel){
		attributeDefaultLabels.set(index, newLabel);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Predicate other = (Predicate) obj;
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		if (this.ariety != other.ariety) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return name;
	}
	
	/**
	 * Returns the name followed by all the attribute labels present in a predicate.
	 * @return an unmodifiable list containing all the attribute labels.
	 */
	public List<String> getPredicateNameAndAttributesLabels(){
		List<String> result = new LinkedList<>();
		result.add(name);
		result.addAll(attributeDefaultLabels);
		return Collections.unmodifiableList(result);
	}

	public boolean labelIsDuplicated(String changedLabel, int index) {
		for(int i=0;i<attributeDefaultLabels.size();i++) {
			if(i!=index && attributeDefaultLabels.get(i).equals(changedLabel)) {
				return true;
			}
		}
		return false;
	}
	
	
}
