/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bl.aspDomain;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class represent an answer set. An answer set is a minimal model of a disjunctive logic program.
 * @author Andrea
 */
public class AnswerSet {
	
	private Map<Predicate,List<Atom>> atoms;
	private Map<String,Predicate> predicates;
	private File file;
	/**
	 * Builds an answer set from a list of atoms.
	 * @param atoms 
	 */
	public AnswerSet (Map<Predicate,List<Atom>> atoms){
		this.atoms = atoms;
		predicates = new HashMap<>();
		for(Predicate predicate: atoms.keySet()) {
			predicates.put(predicate.getName(), predicate);
		}
	}
	
	/**
	 * Returns all the atoms that are part of the answer set.
	 * @return an unmodifiable list containing the atoms.
	 */
	public Collection<List<Atom>> getAtoms(){
		return Collections.unmodifiableCollection(atoms.values());
	}
	
	/**
	 * Returns all the predicates of the answer set.
	 * @return an unmodifiable set of predicates.
	 */
	public Set<Predicate> getPredicates(){
		return Collections.unmodifiableSet(atoms.keySet());
	}
	
	/**
	 * Returns all the attributes in a specified index, belonging to a given predicate.
	 * @param index the position of the desired attribute. 
	 * @param predicate reference predicate for the atom.
	 * @return a unmodifiable list containing all the attributes found.
	 */
	public List<String> getAllAttributesValue (int index, Predicate predicate){
		List<String> result = new ArrayList<>();
		for (Atom currentAtom : atoms.get(predicate)){
			result.add(currentAtom.getAttributeAt(index));
		}
		return Collections.unmodifiableList(result);
	}
	
	public List<Atom> getAtomsFromPredicate (Predicate predicate){
		return Collections.unmodifiableList(atoms.get(predicate));
	}
	
	public Integer getMaxAriety() {
		int maxAriety = 0;
		for(Predicate predicate: atoms.keySet()) {
			if(predicate.getAriety()>maxAriety) {
				maxAriety = predicate.getAriety();
			}
		}
		return maxAriety;
	}

	public Map<String, Predicate> getPredicateMap() {
		return predicates;
	}
	
	public void setRelatedFile(File file) {
		this.file = file;
	}

	public File getRelatedFile() {
		return this.file;
	}
	
	
}
