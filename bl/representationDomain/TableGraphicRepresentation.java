package bl.representationDomain;

import bl.representationDomain.defaultConfigurations.TableDefaultConfiguration;
import bl.aspDomain.Predicate;
import java.util.List;
import java.util.Set;


public class TableGraphicRepresentation extends GraphicRepresentation {

	private TableGraphicDefinition emptyGraphicDefinition;
	private static TableGraphicRepresentation instance;
	private TableDefaultConfiguration tableConfigs;

	public static TableGraphicRepresentation getInstance(){
		if (instance == null){
			instance = new TableGraphicRepresentation();
		}
		return instance;
	}
	
	private TableGraphicRepresentation (){
		emptyGraphicDefinition = new TableGraphicDefinition();
		tableConfigs = TableDefaultConfiguration.getInstance();
	}
	
	@Override
	public TableGraphicDefinition instantiateGraphicDefinition(Set<Predicate> predicates) {
		return emptyGraphicDefinition.clone().initialize(tableConfigs, predicates);
	}

}
