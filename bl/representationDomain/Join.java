package bl.representationDomain;

import java.util.List;


public class Join {
	private String name;
	private List<String> joinAttributes;
	private boolean mergeColumns;

	public Join(String name, List<String> joinAttributes, boolean mergeColumns) {
		this.name = name;
		this.joinAttributes = joinAttributes;
		this.mergeColumns = mergeColumns;
	}	
	
	public String getName() {
		return name;
	}

	public List<String> getJoinAttributes() {
		return joinAttributes;
	}

	public boolean areColumnsMerged() {
		return mergeColumns;
	}
	
	
	
}
