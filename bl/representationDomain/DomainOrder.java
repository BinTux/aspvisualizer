package bl.representationDomain;

/**
 *
 * @author Binno
 */
public enum DomainOrder {
	OCCURRENCE,
	ALPHABETICAL,
	INVERSE_ALPHA,
	NUMERICAL,
	INVERSE_NUMERICAL;
}
