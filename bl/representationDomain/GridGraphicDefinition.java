package bl.representationDomain;

import bl.appState.BlManager;
import bl.aspDomain.AnswerSet;
import bl.representationDomain.defaultConfigurations.GridDefaultConfiguration;
import bl.aspDomain.Predicate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import services.fileManagement.GridDefinitionDirector;


public class GridGraphicDefinition extends GraphicDefinition implements Cloneable {
	private Map<GridAxis, Map<Predicate, Integer>> indexingAttributes;
	private Map<GridAxis, List<String>> userDomains;
	private Map<GridAxis, Map<Predicate,List<Integer>>> answerSetDomains;
	private String background;
	private Map<GridAxis,DomainOrder> domainOrders;
	private boolean attributeOverlapping;
	private Map<Predicate,VisualDescription> visualDescriptions;
	
	public GridGraphicDefinition(){
		//THE HELL!!!!
		indexingAttributes = new HashMap<>();
		indexingAttributes.put(GridAxis.ROW, new HashMap<Predicate,Integer>());
		indexingAttributes.put(GridAxis.COLUMN,new HashMap<Predicate,Integer>());
		answerSetDomains = new HashMap<>();
		answerSetDomains.put(GridAxis.ROW, new HashMap<Predicate,List<Integer>>());
		answerSetDomains.put(GridAxis.COLUMN, new HashMap<Predicate,List<Integer>>());
		domainOrders = new HashMap<>();
		visualDescriptions = new HashMap<>();
		userDomains = new HashMap<>();
		userDomains.put(GridAxis.ROW, new ArrayList<String>());
		userDomains.put(GridAxis.COLUMN, new ArrayList<String>());
	}

	public GridGraphicDefinition(Map<GridAxis, Map<Predicate, Integer>> indexingAttributes, 
									Map<GridAxis, Map<Predicate, List<Integer>>> answerSetDomains, 
									Map<GridAxis, DomainOrder> domainOrders, boolean attributeOverlapping, 
									Map<Predicate, VisualDescription> visualDescriptions) {
		
		this.indexingAttributes = new HashMap<>();
		this.indexingAttributes.put(GridAxis.ROW, new HashMap<Predicate,Integer>());
		this.indexingAttributes.put(GridAxis.COLUMN,new HashMap<Predicate,Integer>());
		this.answerSetDomains = new HashMap<>();
		this.answerSetDomains.put(GridAxis.ROW, new HashMap<Predicate,List<Integer>>());
		this.answerSetDomains.put(GridAxis.COLUMN, new HashMap<Predicate,List<Integer>>());
		this.domainOrders = new HashMap<>();
		this.visualDescriptions = new HashMap<>();
		this.userDomains = new HashMap<>();
		this.userDomains.put(GridAxis.ROW, new ArrayList<String>());
		this.userDomains.put(GridAxis.COLUMN, new ArrayList<String>());
		this.attributeLabels = new HashMap<>();
		
		Map<Predicate,Integer> rowIndexingAttributes = indexingAttributes.get(GridAxis.ROW);
		Map<Predicate,Integer> columnIndexingAttributes = indexingAttributes.get(GridAxis.COLUMN);
	    Map<Predicate,List<Integer>> columnDomainMap = answerSetDomains.get(GridAxis.COLUMN);
		Map<Predicate,List<Integer>> rowDomainMap = answerSetDomains.get(GridAxis.ROW);
		//Deep copying indexing attributes
		for(Map.Entry<Predicate,Integer> currentEntry : rowIndexingAttributes.entrySet()){
			Integer clonedInteger = new Integer(currentEntry.getValue().intValue());
			this.indexingAttributes.get(GridAxis.ROW).put(currentEntry.getKey(),clonedInteger);
		}
		for(Map.Entry<Predicate,Integer> currentEntry : columnIndexingAttributes.entrySet()){
			Integer clonedInteger = new Integer(currentEntry.getValue().intValue());
			this.indexingAttributes.get(GridAxis.COLUMN).put(currentEntry.getKey(),clonedInteger);
		}
		//Deep copying domains attributes
		for (Map.Entry<Predicate,List<Integer>> currentEntry : rowDomainMap.entrySet()){
			List<Integer> clonedList = new ArrayList();
			for(Integer currentOriginalInteger : currentEntry.getValue()){
				clonedList.add(new Integer(currentOriginalInteger.intValue()));
			}
			this.answerSetDomains.get(GridAxis.ROW).put(currentEntry.getKey(), clonedList);
		}
		for (Map.Entry<Predicate,List<Integer>> currentEntry : columnDomainMap.entrySet()){
			List<Integer> clonedList = new ArrayList();
			for(Integer currentOriginalInteger : currentEntry.getValue()){
				clonedList.add(new Integer(currentOriginalInteger.intValue()));
			}
			this.answerSetDomains.get(GridAxis.COLUMN).put(currentEntry.getKey(), clonedList);
		}
		
		this.domainOrders.put(GridAxis.ROW, domainOrders.get(GridAxis.ROW));
		this.domainOrders.put(GridAxis.COLUMN, domainOrders.get(GridAxis.COLUMN));
		this.attributeOverlapping = attributeOverlapping;
		
		for(Map.Entry<Predicate,VisualDescription> currentEntry : visualDescriptions.entrySet()){
			this.visualDescriptions.put(currentEntry.getKey(), visualDescriptions.get(currentEntry.getKey()).clone());
		}
	}
	
	
	
	public GridGraphicDefinition initialize(GridDefaultConfiguration configs, Set<Predicate> predicates){
		//TODO rincontrollare
		Map<Predicate,Integer> columnIndexingMap = indexingAttributes.get(GridAxis.COLUMN);
		Map<Predicate,Integer> rowIndexingMap = indexingAttributes.get(GridAxis.ROW);
		Map<Predicate,List<Integer>> columnDomainMap = answerSetDomains.get(GridAxis.COLUMN);
		Map<Predicate,List<Integer>> rowDomainMap = answerSetDomains.get(GridAxis.ROW);
		List<Integer> defaultRowDomain = new ArrayList<>();
		defaultRowDomain.add(configs.getRowDomainAttribute());
		List<Integer> defaultColumnDomain = new ArrayList<>();
		defaultColumnDomain.add(configs.getColumnDomainAttribute());
		for (Predicate currentPredicate : predicates){
			rowIndexingMap.put(currentPredicate, configs.getRowIndexingAttribute());
			columnIndexingMap.put(currentPredicate, configs.getColumnIndexingAttribute());
			columnDomainMap.put(currentPredicate,defaultColumnDomain);
			rowDomainMap.put(currentPredicate,defaultRowDomain);
			visualDescriptions.put(currentPredicate, configs.getVisualDescription());
		}
		overWriteLabels = configs.isOverwriteLabels();
		domainOrders.put(GridAxis.ROW, configs.getRowDomainOrder());
		domainOrders.put(GridAxis.COLUMN, configs.getColumnDomainOrder());
		attributeOverlapping = configs.isAttributeOverlapping();
		super.initAttributeLabels(predicates);
		return this;
	}

	public Map<Predicate, Integer> getIndexingAttributes(GridAxis gridAxis) {
		return indexingAttributes.get(gridAxis);
	}

	public List<String> getAllDomains(GridAxis gridAxis) {
		if(userDomains.get(gridAxis).isEmpty()) {
			List<String> resultSet= new LinkedList<>();
			for(Predicate predicate : answerSetDomains.get(gridAxis).keySet()) {
				for(Integer i: answerSetDomains.get(gridAxis).get(predicate)) {
					resultSet.addAll(BlManager.getInstance().getAnswerSet().getAllAttributesValue(i,predicate));
				}
			}
			return getOrderedDomain(resultSet, gridAxis);
		}
		return getOrderedDomain(userDomains.get(gridAxis), gridAxis);
		//TODO apply oredering
	}
	
	public int getDomainSize(GridAxis axis){
		Map<Predicate,List<Integer>> domains = answerSetDomains.get(axis);
		int dim = 0;
		for (Predicate currentPredicate : domains.keySet()){
			dim+= domains.get(currentPredicate).size();
		}
		return dim;
	}
	
	public String getBackground() {
		return background;
	}

	public DomainOrder getDomainOrder(GridAxis gridAxis) {
		return domainOrders.get(gridAxis);
	}

	public boolean isAttributeOverlapping() {
		return attributeOverlapping;
	}

	public Map<Predicate, VisualDescription> getVisualDescriptions() {
		return visualDescriptions;
	}
	
	public List<String> getUserDomain(GridAxis gridAxis) {
		return userDomains.get(gridAxis);
	}
	
	public boolean isUserDomainSet(GridAxis gridAxis) {
		return !userDomains.get(gridAxis).isEmpty();
	}
	
	@Override
	public void addDefinitionToVisualizationProduct() {
		GridDefinitionDirector.getInstance().buildGridGraphicDefinitionConfiguration(this);
	}

	private List<String> getOrderedDomain(List<String> strings,GridAxis gridAxis) {
		
		DomainOrder choosenOrder = domainOrders.get(gridAxis);
		switch(choosenOrder) {
			case OCCURRENCE:
				return strings;
			case ALPHABETICAL:
				break;
			case INVERSE_ALPHA:
				break;
			case NUMERICAL:
				break;
			case INVERSE_NUMERICAL:
				break;				
		}
		return strings;
	}
	@Override
	public GridGraphicDefinition clone(){
		return new GridGraphicDefinition(indexingAttributes,answerSetDomains,domainOrders,attributeOverlapping,visualDescriptions);
	}
	public Map<Predicate, List<Integer>> getAnswerSetDomains(GridAxis axis) {
		return answerSetDomains.get(axis);
	}

	public Map<GridAxis, Map<Predicate, Integer>> getIndexingAttributes() {
		return indexingAttributes;
	}

	public Map<GridAxis, List<String>> getUserDomains() {
		return userDomains;
	}

	public Map<GridAxis, Map<Predicate, List<Integer>>> getAnswerSetDomains() {
		return answerSetDomains;
	}

	public Map<GridAxis, DomainOrder> getDomainOrders() {
		return domainOrders;
	}
	
	public void setDomains(GridAxis type, Map<Predicate,List<Integer>> newDomains){
		answerSetDomains.put(type, newDomains);
	}
	
	public void setIndexingAttribute(GridAxis type,String predicate, Integer attribute){
		//System.out.println(attribute);
		indexingAttributes.get(type).put(BlManager.getInstance().getPredicateFromName(predicate),attribute);
	}
	
	public void setVisualDescription(String predicate,int newDescriptor){
		this.visualDescriptions.get(BlManager.getInstance().getPredicateFromName(predicate)).setNewDescriptor(newDescriptor);
	}
	
	
}

