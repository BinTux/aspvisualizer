package bl.representationDomain;

import bl.appState.BlManager;
import bl.representationDomain.defaultConfigurations.TableDefaultConfiguration;
import bl.aspDomain.Predicate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import services.fileManagement.TableDefinitionDirector;


public class TableGraphicDefinition extends GraphicDefinition implements Cloneable {
	private boolean aJoinIsSet;
	private Join join;

	public TableGraphicDefinition() {
		attributeLabels = new HashMap<>();
	}
	
	public boolean hasJoinBeenSet() {
		return aJoinIsSet;
	}

	public Join getJoin() {
		return join;
	}


	@Override
	public void addDefinitionToVisualizationProduct() {
		TableDefinitionDirector.getInstance().buildTableGraphicDefinitionConfiguration(this);
	}
	
	@Override
	public TableGraphicDefinition clone(){
		TableGraphicDefinition clone = new TableGraphicDefinition();
		clone.setWillLabelBeOverwritten(this.overWriteLabels);
		return clone;
	}
	
	public TableGraphicDefinition initialize(TableDefaultConfiguration defaultConfiguration, Set<Predicate> predicates) {
		super.initAttributeLabels(predicates);
		return this;
	}

	public boolean labelIsDuplicated(String changedLabel, int column, String changedPredicate) {
		List<String> labels = attributeLabels.get(BlManager.getInstance().getPredicateFromName(changedPredicate));
		for(int i=0;i<labels.size();i++) {
			if(i!=column && labels.get(i).equals(changedLabel)) {
				return true;
			}
		}
		return false;
	}

	public void setPredicateAttributeLabelTo(String changedPredicate, int column, String changedLabel) {
		attributeLabels.get(BlManager.getInstance().getPredicateFromName(changedPredicate)).set(column, changedLabel);
	}

	public void setJoin(Join join) {
		this.join = join;
		aJoinIsSet = true;
	}
}
