/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bl.representationDomain.defaultConfigurations;

import bl.representationDomain.DomainOrder;
import bl.representationDomain.VisualDescription;
import services.fileManagement.XmlReader;

/**
 *
 * @author Andrea
 */
public class GridDefaultConfiguration {
	
	private int rowIndexingAttribute;
	private int columnIndexingAttribute;
	private int rowDomainAttribute;
	private int columnDomainAttribute;
	private boolean overwriteLabels;
	private DomainOrder rowDomainOrder;
	private DomainOrder columnDomainOrder;
	private boolean attributeOverlapping;
	private VisualDescription visualDescription;
        private static GridDefaultConfiguration instance;
        
        public static GridDefaultConfiguration getInstance(){
            if (instance == null){
                instance = new GridDefaultConfiguration();
            }
            return instance;
        }
	
	private GridDefaultConfiguration() {
		XmlReader reader	    = new XmlReader("configs/grid_default_configuration.xml");
		rowIndexingAttribute    = reader.getIntContent("rowIndexingAttribute");
		columnIndexingAttribute = reader.getIntContent("columnIndexingAttribute");
		rowDomainAttribute      = reader.getIntContent("rowDomainAttribute");
		columnDomainAttribute   = reader.getIntContent("columnDomainAttribute");
		overwriteLabels         = reader.getBooleanContent("overwriteLabels");
		rowDomainOrder			= DomainOrder.valueOf(reader.getStringContent("rowDomainOrder").toUpperCase());
		columnDomainOrder		= DomainOrder.valueOf(reader.getStringContent("columnDomainOrder").toUpperCase());
		attributeOverlapping	= reader.getBooleanContent("attributeOverlapping");
		XmlReader visualDescriptionReader = reader.getXMLReader("visualDescription");
		int fieldIdentifier		= visualDescriptionReader.getIntContent("fieldIdentifier");
		boolean imageAssociation = visualDescriptionReader.getBooleanContent("imageAssociation");
		visualDescription		= new VisualDescription(imageAssociation,fieldIdentifier);
		
		
	}

	public int getRowIndexingAttribute() {
		return rowIndexingAttribute;
	}

	public int getColumnIndexingAttribute() {
		return columnIndexingAttribute;
	}

	public int getRowDomainAttribute() {
		return rowDomainAttribute;
	}

	public int getColumnDomainAttribute() {
		return columnDomainAttribute;
	}

	public boolean isOverwriteLabels() {
		return overwriteLabels;
	}

	public DomainOrder getRowDomainOrder() {
		return rowDomainOrder;
	}

	public DomainOrder getColumnDomainOrder() {
		return columnDomainOrder;
	}

	public boolean isAttributeOverlapping() {
		return attributeOverlapping;
	}

	public VisualDescription getVisualDescription() {
		return visualDescription;
	}
	
	
}
