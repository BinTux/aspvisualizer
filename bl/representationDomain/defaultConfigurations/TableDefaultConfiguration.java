/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bl.representationDomain.defaultConfigurations;

import services.fileManagement.XmlReader;

/**
 *
 * @author Andrea
 */
public class TableDefaultConfiguration {
	
	private boolean overwriteLabels;
	private static TableDefaultConfiguration instance;

	public static TableDefaultConfiguration getInstance(){
		if (instance == null){
			instance = new TableDefaultConfiguration();
		}
		return instance;
	}
	
	private  TableDefaultConfiguration(){
		XmlReader reader = new XmlReader("configs/table_default_configuration.xml");
		overwriteLabels = reader.getBooleanContent("overwriteLabels");
	}

	public boolean isOverwriteLabels() {
		return overwriteLabels;
	}
	
}
