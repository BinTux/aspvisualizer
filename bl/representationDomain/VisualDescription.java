package bl.representationDomain;


public class VisualDescription {
	private boolean imageAssociation;
	private int fieldIdentifier;

	public VisualDescription(boolean imageAssociation, int fieldIdentifier){
		this.imageAssociation = imageAssociation;
		this.fieldIdentifier = fieldIdentifier;
	}
	
	public boolean isAnImageAssociated() {
		return imageAssociation;
	}

	public int getFieldIdentifier() {
		return fieldIdentifier;
	}
	
	@Override
	public VisualDescription clone() {
		return new VisualDescription(imageAssociation, fieldIdentifier);
	}
	
	public void setNewDescriptor(int newIndex){
		this.fieldIdentifier = newIndex;
	}
	
}
