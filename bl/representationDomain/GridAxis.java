package bl.representationDomain;


public enum GridAxis {
	ROW("Row"),
	COLUMN("Column");
	
	private String name;
	
	private GridAxis(String name){
		this.name = name;
	}
	
	@Override
	public String toString(){
		return this.name;
	}
}
