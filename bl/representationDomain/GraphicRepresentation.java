package bl.representationDomain;

import bl.aspDomain.Predicate;
import java.util.List;
import java.util.Set;


public abstract class GraphicRepresentation {
	
	protected String name;
	protected int minimumArietyConstraint;
	public abstract GraphicDefinition instantiateGraphicDefinition(Set<Predicate> predicates);

	public String getName() {
		return name;
	}

	public int getMinimumArietyConstraint() {
		return minimumArietyConstraint;
	}
	

}
