package bl.representationDomain;

import bl.appState.BlManager;
import bl.aspDomain.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class GraphicDefinition {
	
	protected String name;
	protected boolean overWriteLabels;
	protected Map<Predicate,List<String>> attributeLabels;


	public boolean willLabelsBeOverwritten() {
		return overWriteLabels;
	}
	protected void setWillLabelBeOverwritten(boolean labelsOverwritten){
		this.overWriteLabels = labelsOverwritten;
	}
	
	public List<String> getPredicateLabels(Predicate predicate ) {
		return this.attributeLabels.get(predicate);
	}
	public List<String> getPredicateLabels(String predicate ) {
		return this.attributeLabels.get(BlManager.getInstance().getPredicateFromName(predicate));
	}
	
	public Map<Predicate,List<String>> getAttributeLabels() {
		return attributeLabels;
	}
	
	public boolean labelIsDuplicated(String changedLabel, int column, String changedPredicate) {
		List<String> labels = attributeLabels.get(BlManager.getInstance().getPredicateFromName(changedPredicate));
		for(int i=0;i<labels.size();i++) {
			if(i!=column && labels.get(i).equals(changedLabel)) {
				return true;
			}
		}
		return false;
	}
	
	public void setPredicateAttributeLabelTo(String changedPredicate, int column, String changedLabel) {
		attributeLabels.get(BlManager.getInstance().getPredicateFromName(changedPredicate)).set(column, changedLabel);
	}
	
	protected void initAttributeLabels(Set<Predicate> predicates){
		for(Predicate predicate: predicates) {
			List<String> labels = predicate.getAttributesLabels();
			List<String> newLabels = new ArrayList<>();
			for(String label: labels) {
				newLabels.add(new String(label.toCharArray()));
			}
			attributeLabels.put(predicate, newLabels);
		}
	}
	
	public abstract void addDefinitionToVisualizationProduct();

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
