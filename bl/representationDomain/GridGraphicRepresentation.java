package bl.representationDomain;

import bl.representationDomain.defaultConfigurations.GridDefaultConfiguration;
import bl.aspDomain.Predicate;
import java.util.List;
import java.util.Set;


public class GridGraphicRepresentation extends GraphicRepresentation {

	private static GridGraphicDefinition emptyGraphicDefinition;
	private static GridGraphicRepresentation instance;
	private static GridDefaultConfiguration gridConfigs;
	
	public static GridGraphicRepresentation getInstance(){
		if (instance == null){
			instance = new GridGraphicRepresentation();			
		}
		return instance;
	}
	
	private GridGraphicRepresentation(){
		emptyGraphicDefinition = new GridGraphicDefinition();
		gridConfigs = GridDefaultConfiguration.getInstance();
	}
	
	@Override
	public GridGraphicDefinition instantiateGraphicDefinition(Set<Predicate> predicates) {
		//TODO controllo arietà
		return emptyGraphicDefinition.clone().initialize(gridConfigs,predicates);
	}
	
}
