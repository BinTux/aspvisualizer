
package bl.appState;

import mediators.MainInterfaceManager;
import bl.aspDomain.AnswerSet;
import bl.aspDomain.Atom;
import bl.aspDomain.Predicate;
import bl.representationDomain.GraphicDefinition;
import bl.representationDomain.GraphicRepresentation;
import bl.representationDomain.GridGraphicRepresentation;
import bl.representationDomain.TableGraphicRepresentation;
import it.unical.mat.dlv.parser.ParseException;
import it.unical.mat.wrapper.exception.NoModelParseException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import services.fileManagement.GridDefinitionDirector;
import services.dlpParsing.Parser;
import services.fileManagement.TableDefinitionDirector;
import services.fileManagement.XmlBuilder;


public class BlManager {
	private List<GraphicDefinition> graphicDefinitions;
	private GraphicDefinition currentDefinition;
	private AnswerSet answerSet;
	private static BlManager instance;
	private int gridDefinitionsNumber = 0;
			
	public static BlManager getInstance() {
		if(instance==null)  {
			instance = new BlManager();
		}
		return instance;
	}
	
	private BlManager() {
		graphicDefinitions = new LinkedList<>();
		GridDefinitionDirector.getInstance().initGridDefinitionDirector(XmlBuilder.getInstance());
		TableDefinitionDirector.getInstance().initTableDefinitionDirector(XmlBuilder.getInstance());
		GridGraphicRepresentation.getInstance();
		TableGraphicRepresentation.getInstance();
	}

	public List<GraphicDefinition> getGraphicDefinitions() {
		return graphicDefinitions;
	}

	public AnswerSet getAnswerSet() {
		return answerSet;
	}
	
	public void generateVisualizationFile(File file) {
		XmlBuilder.getInstance().inizializeProduct();
		for(GraphicDefinition graphicDefinition: graphicDefinitions) {
			graphicDefinition.addDefinitionToVisualizationProduct();
		}
		XmlBuilder.getInstance().writeProduct(file);
	}
	
	/**
	 * Adds the current definition to the list of graphic definitions.
	 * This method is called when a definition is completed by the user.
	 */
	public void addGraphicDefinition() {
		graphicDefinitions.add(currentDefinition);
	}
	
	/**
	 * Create an instance of a graphic definition from a set of chosen predicates
	 * and the wanted representation.
	 * @param predicates Chosen predicates, which will be take part to the graphic definition creation process.
	 * @param representation The representation to apply to creating the graphic definition.
	 */
	public void createGraphicDefinition(Set<Predicate> predicates, GraphicRepresentation representation){
		GraphicDefinition graphicDefinition = representation.instantiateGraphicDefinition(predicates);
		currentDefinition = graphicDefinition;
	}
	
	public void submitAnswerSet(File file) {
		try {
			Parser.getInstance().parse(file);
			answerSet = Parser.getInstance().getAnswerSet();
			answerSet.setRelatedFile(file);
			MainInterfaceManager.getInstance().answerSetSubmitted(answerSet);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (ParseException ex) {
			ex.printStackTrace();
		} catch (NoModelParseException ex) {
			ex.printStackTrace();
		}
	}
	
	public List<Atom> getAtomsFromPredicate (Predicate predicate){
		return answerSet.getAtomsFromPredicate(predicate);
	}
	
	public Predicate getPredicateFromName (String predicateName){
		return answerSet.getPredicateMap().get(predicateName);
	}
	
	public GraphicDefinition getCurrentGraphicDefinition(){
		return currentDefinition;
	}

	public void overWriteLabels(Map<Predicate, List<String>> attributeLabels) {
		for(Predicate predicate: attributeLabels.keySet()) {
			List<String> attributes = attributeLabels.get(predicate);
			answerSet.getPredicateMap().get(predicate.getName()).setAttributesLabels(attributes);
		}
		MainInterfaceManager.getInstance().answerSetSubmitted(answerSet);
	}
	
	public int getGridDefinitionNumber(){
		return gridDefinitionsNumber++;
	}
}
