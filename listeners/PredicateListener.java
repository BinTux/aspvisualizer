/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listeners;

import bl.aspDomain.Predicate;

/**
 *
 * @author Andrea
 */
public interface PredicateListener {
	
	void predicateChanged(String predicateName);
}
