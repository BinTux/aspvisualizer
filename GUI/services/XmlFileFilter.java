
package GUI.services;

import java.io.File;
import javax.swing.filechooser.FileFilter;


class XmlFileFilter extends FileFilter {
	
	@Override
	public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
        return file.getName().endsWith(".xml");
	}

	@Override
	public String getDescription() {
		return "Xml File";
	}

}
