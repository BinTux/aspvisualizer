
package GUI.services;

import java.io.File;
import javax.swing.filechooser.FileFilter;


public class DlvFileFilter extends FileFilter{

	@Override
	public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
        return file.getName().endsWith("dl");
	}

	@Override
	public String getDescription() {
		return "Dlp File";
	}

}
