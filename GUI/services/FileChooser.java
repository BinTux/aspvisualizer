
package GUI.services;

import mediators.MainInterfaceManager;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


public class FileChooser extends JFileChooser {
	private static FileChooser instance;
	
	public static FileChooser getInstance() {
		if(instance == null) {
			instance = new FileChooser();
		}
		return instance;
	}
	private FileChooser() {
		super();
		   
	}	
	 @Override
    public void approveSelection(){
        File f = getSelectedFile();
        if(f.exists() && getDialogType() == SAVE_DIALOG){    
			int result = JOptionPane.showConfirmDialog(this,"The file exists, overwrite?","Existing file",JOptionPane.YES_NO_CANCEL_OPTION);
            switch(result){
                case JOptionPane.YES_OPTION:
                    super.approveSelection();
                    return;
                case JOptionPane.NO_OPTION:
                    return;
                case JOptionPane.CLOSED_OPTION:
                    return;
                case JOptionPane.CANCEL_OPTION:
                    cancelSelection();
                    return;
            }
        }
		super.approveSelection();
		
    }

	public void chooseDlpFile() {
		setFileFilter(new DlvFileFilter());
		chooseOpenFile();
	}

	private void chooseOpenFile() {
		int n = showOpenDialog(this);
		File file  = null;
		if (n == JFileChooser.APPROVE_OPTION) {
			file = getSelectedFile();
			MainInterfaceManager.getInstance().submitChoosenFile(file);
		}
	}
	
	public void chooseSaveFile() {
		setSaveFilter();
		int n = showSaveDialog(this);
		File file = null;
		if(n == JFileChooser.APPROVE_OPTION) {
			file = getSelectedFile();
			file = applySaveExstension(file);
			
			
			MainInterfaceManager.getInstance().saveToFile(file);
		}
	}

	private void setSaveFilter() {
		setFileFilter(new XmlFileFilter());
	}

	private File applySaveExstension(File saveFile) {
		if(!saveFile.getName().endsWith(".xml")) {
			String withCorrectExtensionFile = saveFile.getPath().concat(".xml");
			File properFile = new File(withCorrectExtensionFile);
			return properFile;
		}
		return saveFile;
	}
}
