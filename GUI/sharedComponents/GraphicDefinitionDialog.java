/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.sharedComponents;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 *
 * @author Andrea
 */
public abstract class GraphicDefinitionDialog extends JDialog{
	
	private PredicateList predicateList;
	private AtomsTable atomsTable;
	protected JFrame ownerFrame;

	public GraphicDefinitionDialog(JFrame owner){
		super(owner,true);
		this.ownerFrame = owner;
	}
	
	public void init(PredicateList list, AtomsTable atomsTable){
		this.predicateList = list;
		this.atomsTable = atomsTable;
	}
	
	public AtomsTable getAtomsTable(){
		return atomsTable;
	}
	
	//public abstract PredicateList getPredicateList();
	
	public PredicateList getPredicateList(){
		return predicateList;
	}
	
	public JFrame getOwnerFrame(){
		return ownerFrame;
	}
	
}
