/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.sharedComponents;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Andrea
 */
public interface AtomsTable {
	
	void setModel(DefaultTableModel model);
	void setColumnLabels(String [] labels);
	
}
