
package GUI.sharedComponents;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import mediators.GraphicInterfaceManager;
import mediators.TableInterfaceManager;


public class ChangeLabelsDialog extends JDialog {
	private JButton applyButton;
	private JButton cancelButton;
	private JTable table;
	private JScrollPane scrollPane;
	private String modifingPredicate;
	private GraphicInterfaceManager mediator;
	
	public ChangeLabelsDialog(Frame owner, GraphicInterfaceManager mediator) {
		super(owner, true);
		this.mediator = mediator;
	}
	
	public void init(List<String> labels, String predicate) {
		modifingPredicate = predicate;
		
		List<String> headers = new LinkedList<>();
		for(int i=1;i<=labels.size();i++) {
			headers.add("Attribute "+i);
		}
		
		DefaultTableModel tableModel = new DefaultTableModel(headers.toArray(), 0);
		tableModel.setColumnIdentifiers(headers.toArray());
		table = new JTable(tableModel);	
		tableModel.addRow(labels.toArray());
		scrollPane = new JScrollPane(table);
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		this.setTitle("Label customization for "+ predicate);
		applyButton = new JButton("Apply");
		cancelButton = new JButton("Cancel");
		initButtonEvents();
		initTableEvents();
		setLocation(200, 150);
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addComponent(scrollPane)
				.addGroup(layout.createSequentialGroup()				
					.addComponent(applyButton)
					.addComponent(cancelButton)));
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(scrollPane, 100, 100, GroupLayout.PREFERRED_SIZE)
				.addGroup(layout.createParallelGroup()				
					.addComponent(applyButton)
					.addComponent(cancelButton)));
		pack();
		setVisible(true);
	}
	
	private void initButtonEvents() {
		applyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mediator.saveModifiedLabelsOnDefinition();
				mediator.closeEditLabelsDialog();
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mediator.closeEditLabelsDialog();
			}
		});
	}

	public String getModifingPredicate() {
		return modifingPredicate;
	}

	public JTable getTable() {
		return table;
	}	
	
	private void initTableEvents() {
		table.getModel().addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent e) {
				int column = e.getColumn();
				mediator.labelChanged(0, column);
			}
		});
	}
}
