package GUI.sharedComponents;

import javax.swing.ListModel;

public interface PredicateList {
	
	String getCurrentSelection();
	ListModel getModel();
}
