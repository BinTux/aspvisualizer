
package GUI.sharedComponents;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import mediators.TableInterfaceManager;


public class PredicateTablePopup extends JPopupMenu {
	private JMenuItem item;
	public PredicateTablePopup() {
		super();
		init();
	}

	private void init() {
		item = new JMenuItem("Change predicate labels");
		super.add(item);
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TableInterfaceManager.getInstance().changeLabelsDialogRequest();
			}
		});
	}
	
	

}
