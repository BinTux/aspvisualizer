
package GUI.tableInterface;

import GUI.sharedComponents.PredicateList;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import mediators.TableInterfaceManager;


public class TablePredicateList extends JPanel implements PredicateList{

	private JList predicateList;
	private JScrollPane scrollPane;
	
	public TablePredicateList(List<String> names) {
		super();
		init(names);
	}

	private void init(List<String> names) {
		this.setBorder(new TitledBorder("Predicate list"));
		predicateList = new JList(names.toArray());	
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(predicateList);
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		predicateList.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		initEvents();
		layout.setHorizontalGroup(layout.createParallelGroup()
				.addComponent(scrollPane));
		layout.setVerticalGroup(layout.createParallelGroup()
				.addComponent(scrollPane));
		
	}

	private void initEvents() {
		predicateList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!e.getValueIsAdjusting()) {
					TableInterfaceManager.getInstance().updateSelectedPredicates();
				}
			}
			
		});
	}

	public JList getPredicateList() {
		return predicateList;
	}

	@Override
	public String getCurrentSelection() {
		return (String)predicateList.getSelectedValue();
	}

	@Override
	public ListModel getModel() {
		return predicateList.getModel();
	}
	
	

	
	
	
}
 
