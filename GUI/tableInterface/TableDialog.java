
package GUI.tableInterface;

import GUI.sharedComponents.GraphicDefinitionDialog;
import GUI.sharedComponents.PredicateList;
import bl.aspDomain.Predicate;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import mediators.TableInterfaceManager;


public class TableDialog extends GraphicDefinitionDialog{

	private JoinPanel joinPanel;
	private TablePredicateList predicateList;
	private PredicateTable predicateTable;
	private JButton okButton;
	private JButton cancelButton;
	
	public TableDialog(JFrame frame) {
		super(frame);
	}
	
	public void init(List<String> predicates) {
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		this.setTitle("Table customization");
		this.joinPanel = new JoinPanel();
		this.predicateList = new TablePredicateList(predicates);	
		this.predicateTable = new PredicateTable();		
		this.okButton = new JButton("Create definition");
		this.cancelButton = new JButton("Cancel");
		initButtonEvents();
		super.init(predicateList, predicateTable);
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(layout.createSequentialGroup()
					.addComponent(predicateList,200,GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
					.addComponent(joinPanel))
				.addComponent(predicateTable)
				.addGroup(layout.createSequentialGroup()
					.addComponent(okButton)
					.addComponent(cancelButton)));
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(predicateList,200,GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
					.addComponent(joinPanel))
				.addComponent(predicateTable)
				.addGroup(layout.createParallelGroup()
					.addComponent(okButton)
					.addComponent(cancelButton)));
		pack();
		setVisible(true);
	}

	public void changeJoinStatus(boolean joinChecked) {
		joinPanel.setComponentsStatusAfterJoinCheck(joinChecked);
		
	}

	@Override
	public PredicateList getPredicateList() {
		return predicateList;
	}

	public PredicateTable getPredicateTable() {
		return predicateTable;
	}

	public JFrame getOwnerFrame() {
		return ownerFrame;
	}

	private void initButtonEvents() {
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TableInterfaceManager.getInstance().submitDefinition();
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TableInterfaceManager.getInstance().cancelDefinition();
			}
		});
	}

	public JoinPanel getJoinPanel() {
		return joinPanel;
	}

	public boolean isOverwriteLabels() {
		return predicateTable.getOverwriteCheckBox().isSelected();
	}
	
}
