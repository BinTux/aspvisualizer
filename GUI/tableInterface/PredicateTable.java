
package GUI.tableInterface;

import GUI.sharedComponents.AtomsTable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import mediators.TableInterfaceManager;


public class PredicateTable extends JPanel implements AtomsTable {
	private JTable predicateTable;
	private JScrollPane scrollPane;
	private JCheckBox overwriteCheckBox;
	
	public PredicateTable() {
		super();
		init();
	}
	
	private void init(){
		setBorder(new TitledBorder("Labels"));
		predicateTable = new JTable();	
		predicateTable.setModel(new DefaultTableModel());
		predicateTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);	
		predicateTable.getTableHeader().setReorderingAllowed(false);
		scrollPane = new JScrollPane(predicateTable);
		overwriteCheckBox = new JCheckBox("Overwrite labels on exit");
		overwriteCheckBox.setSelected(true);
		initPopup();
		
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addComponent(scrollPane)
				.addComponent(overwriteCheckBox));
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(scrollPane,100,150,Short.MAX_VALUE)
				.addComponent(overwriteCheckBox));
		
	}
	public void replaceColumnTitle(int index, String value) {
		predicateTable.getColumnModel().getColumn(index).setHeaderValue(value);	
	}
	public void replaceElement(int rowIndex, int columnIndex, String value) {
		predicateTable.getModel().setValueAt(value, rowIndex, columnIndex);
	}
	
	public JTable getTable() {
		return predicateTable;
	}

	private void initPopup() {
		final JPopupMenu popupMenu= new JPopupMenu();          
        JMenuItem rename= new JMenuItem("Rename labels");
        rename.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TableInterfaceManager.getInstance().changeLabelsDialogRequest();
			}
		});
        popupMenu.add(rename);
		predicateTable.getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if(mouseEvent.getButton()== MouseEvent.BUTTON3 ) {
					popupMenu.show(predicateTable, mouseEvent.getX(), mouseEvent.getY());
				}
		  
			};
		});
	}

	@Override
	public void setModel(DefaultTableModel model) {
		predicateTable.setModel(model);
	}

	@Override
	public void setColumnLabels(String[] labels) {
		((DefaultTableModel)predicateTable.getModel()).setColumnIdentifiers(labels);
	}

	public JCheckBox getOverwriteCheckBox() {
		return overwriteCheckBox;
	}
	
	
}
