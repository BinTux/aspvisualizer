
package GUI.tableInterface;

import mediators.TableInterfaceManager;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;


public class JoinPanel extends JPanel {

	private JCheckBox joinCheck;
	private JCheckBox mergeCheck;
	private JLabel nameLabel;
	private JLabel joinAttributesLabel;
	private JTextField nameField;
	private JList attributeList;
	private JScrollPane attributeScroll;	
	private JLabel addingLabel;
	private JTextField addingField;
	private JButton addButton;
	private JButton removeButton;
			 
	public JoinPanel() {
		super();
		init();
	}
	
	private void init() {
		this.setBorder(new TitledBorder("Join properties"));
		this.joinCheck = new JCheckBox("set a join");
		joinCheck.setSelected(true);
		this.mergeCheck = new JCheckBox("merge join columns");
		mergeCheck.setSelected(true);
		this.nameLabel = new JLabel("name:");
		this.nameField = new JTextField();
		this.joinAttributesLabel = new JLabel("Join attributes");
		DefaultListModel<String> listModel = new DefaultListModel<>();
		this.attributeList = new JList(listModel);
		attributeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.attributeScroll = new JScrollPane(attributeList);
		this.addingLabel = new JLabel("Adding attribute:");
		this.addingField = new JTextField();
		this.addButton = new JButton("add to list");
		this.removeButton = new JButton("remove from list");
		initButtonEvents();
				
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(joinCheck)
					.addComponent(nameLabel)
					.addComponent(nameField, 100, 100, 200)
					.addComponent(joinAttributesLabel)
					.addComponent(attributeScroll)
					.addGroup(layout.createSequentialGroup()
						.addComponent(removeButton)
						.addComponent(mergeCheck)))
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(addingLabel)
					.addComponent(addingField)
					.addComponent(addButton)));
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(joinCheck)
				.addComponent(nameLabel)
				.addComponent(nameField,GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addGroup(layout.createSequentialGroup()
						.addComponent(joinAttributesLabel)
						.addComponent(attributeScroll)
						.addGroup(layout.createParallelGroup()
							.addComponent(removeButton)
							.addComponent(mergeCheck)))
					.addGroup(layout.createSequentialGroup()
						.addComponent(addingLabel)
						.addComponent(addingField, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(addButton))));
		
		initJoinCheckBox();
	}

	private void initJoinCheckBox() {
		joinCheck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox check = (JCheckBox) e.getSource();
				TableInterfaceManager.getInstance().joinActivated(check.isSelected());
			}
		});
	}
	
	public void setComponentsStatusAfterJoinCheck(boolean enable) {
		
		for(Component c: this.getComponents()) {
			if(c != joinCheck) {
				c.setEnabled(enable);
			}
		}
	}

	public boolean isJoinSelected() {
		return joinCheck.isSelected();
	}

	public String getJoinName() {
		return nameField.getText();
	}

	public JList getAttributeList() {
		return attributeList;
	}

	public JTextField getAddingField() {
		return addingField;
	}

	private void initButtonEvents() {
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TableInterfaceManager.getInstance().addJoinAttributeRequest();
			}
		});
		removeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TableInterfaceManager.getInstance().removeJoinAttributeRequest();
			}
		});
	}

	public boolean isMergeColumn() {
		return mergeCheck.isSelected();
	}
	
	
	
}
