
package GUI;

import bl.appState.BlManager;
import mediators.MainInterfaceManager;


public class Launcher {
	
	public static void main(String[] args) {
		BlManager blManager = BlManager.getInstance();
		MainInterfaceManager interfaceManager = MainInterfaceManager.getInstance();
		interfaceManager.applicationLaunched();
	}
}
