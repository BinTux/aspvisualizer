
package GUI.mainInterface;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;


public class DefinitionsPanel extends JScrollPane{

	private JList predicateList;
	public DefinitionsPanel() {
		super();
	}
	
	public void init(){
		predicateList = new JList();
		predicateList.setModel(new DefaultListModel());
		this.setViewportView(predicateList);
	}

	public JList getPredicateList() {
		return predicateList;
	}	

}
