
package GUI.mainInterface;

import GUI.mainInterface.specialDialogs.EditAnswerSetDialog;
import mediators.MainInterfaceManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class PredicatesPanelAndOptions extends JPanel{
	
	private PredicatesPanel predicatesPanel;
	private JButton editAnswerSetButton;
	private JButton editLabelsButton;
	private JLabel label;

	public PredicatesPanelAndOptions() {
		super();
	}
	
	public void init() {
		this.predicatesPanel = new PredicatesPanel();
		predicatesPanel.init();
		this.add(predicatesPanel);
		label = new JLabel("Predicates list");
		this.add(label);
		editAnswerSetButton = new JButton("Edit answerSet...");
		initEditAnswerSetEvents();
		editLabelsButton = new JButton("Edit labels...");
		this.add(editAnswerSetButton);
		this.add(editLabelsButton);
		editAnswerSetButton.setEnabled(false);
		editLabelsButton.setEnabled(false);
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(label)
				.addComponent(predicatesPanel)
				.addGroup(layout.createSequentialGroup()
					.addComponent(editAnswerSetButton)
					.addComponent(editLabelsButton))
				);
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(label)
				.addComponent(predicatesPanel)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(editAnswerSetButton)
					.addComponent(editLabelsButton))
				);
		layout.linkSize(editAnswerSetButton, editLabelsButton);
	}

	public PredicatesPanel getPredicatesPanel() {
		return predicatesPanel;
	}

	private void initEditAnswerSetEvents() {
		this.editAnswerSetButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EditAnswerSetDialog dialog = new EditAnswerSetDialog(MainInterfaceManager.getInstance().getFrame());
				MainInterfaceManager.getInstance().getFrame().setEditAnswerSetDialog(dialog);
				dialog.init();						
			}
		});
	}
	
	public void activateButtons() {
		this.editAnswerSetButton.setEnabled(true);
		this.editAnswerSetButton.setEnabled(true);
	}
	
	
}
