package GUI.mainInterface;

import bl.appState.BlManager;
import mediators.MainInterfaceManager;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;


public class IconToolBar extends JToolBar{
	private JButton newButton;
	private JButton submitAnswerSetButton;
	private JButton saveButton;
	private JButton settingsButton;
	private JButton visualizeAnswerSetButton;
	
	public IconToolBar() {
		super();
	}

	public void init() {
		this.setFloatable(false);		
		newButton = createDefaultButton("New document");
		submitAnswerSetButton = createDefaultButton("Submit answer set");		
		saveButton = createDefaultButton("Save");
		settingsButton = createDefaultButton("Settings");
		visualizeAnswerSetButton = createDefaultButton("Visualize answer set");
		this.add(newButton);
		this.add(submitAnswerSetButton);
		this.add(saveButton);
		this.add(settingsButton);		
		this.add(visualizeAnswerSetButton);
		this.setRollover(true);
		initActionListeners();
	}
	
	private JButton createDefaultButton(String info) {
		JButton button = new JButton(new ImageIcon("assets/icons/"+info+".png"));
		button.setPreferredSize(new Dimension(40, 40));
		button.setToolTipText(info);
		button.setBorderPainted(false);
		return button; 
	}

	private void initActionListeners() {				
		submitAnswerSetButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MainInterfaceManager.getInstance().submitAnswerSetRequest();
			}
		});
		visualizeAnswerSetButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MainInterfaceManager.getInstance().saveToFileRequest();
			}
		});
	}
	
}
