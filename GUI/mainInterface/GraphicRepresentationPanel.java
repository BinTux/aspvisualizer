
package GUI.mainInterface;

import GUI.gridGraphicInterface.GridDefinitionPanel;
import mediators.MainInterfaceManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;


public class GraphicRepresentationPanel extends JPanel {

	private JButton gridButton;
	private JButton tableButton;
	public GraphicRepresentationPanel() {
		super();
	}
	
	public void init() {
		initButtons();
		gridButton.setToolTipText("Add a grid definition");
		tableButton.setToolTipText("Add a table definition");
		this.add(gridButton);
		this.add(tableButton);		
		this.setBorder(new TitledBorder("Graphic representation"));
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);		
		layout.setHorizontalGroup(layout.createParallelGroup().
			addGroup(layout.createSequentialGroup()
				.addComponent(gridButton)
				.addComponent(tableButton)));
		layout.setVerticalGroup(layout.createSequentialGroup().
			addGroup(layout.createParallelGroup()
				.addComponent(gridButton)
				.addComponent(tableButton)));
		
		disactivatePanel();
	}

	public void activatePanel() {
		this.gridButton.setEnabled(true);
		this.tableButton.setEnabled(true);
	}
	
	public void disactivatePanel() {
		this.gridButton.setEnabled(false);
		this.tableButton.setEnabled(false);
	}
	
	public void disactivateGrid()  {
		this.gridButton.setEnabled(false);
	}
	
	private void initButtons(){
		gridButton = new JButton(new ImageIcon("assets/icons/Grid.png"));
		gridButton.setActionCommand("grid");
		gridButton.addActionListener(new GraphicDefinitionActionListener());
		
		tableButton = new JButton(new ImageIcon("assets/icons/Table.png"));
		tableButton.setActionCommand("table");
		tableButton.addActionListener(new GraphicDefinitionActionListener());
	}
	
	private class GraphicDefinitionActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals(gridButton.getActionCommand())){
				MainInterfaceManager.getInstance().addGridGraphicDefinition();
			}else if (e.getActionCommand().equals(tableButton.getActionCommand())) {
				MainInterfaceManager.getInstance().addTableGraphicDefinition();
			}
		}
		
	}
}
