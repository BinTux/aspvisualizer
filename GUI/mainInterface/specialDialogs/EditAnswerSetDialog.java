
package GUI.mainInterface.specialDialogs;

import mediators.MainInterfaceManager;
import bl.appState.BlManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class EditAnswerSetDialog extends JDialog {
	//TODO modifiche all'answer set quando c'è qualche definizione?
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton applyButton;
	private JButton cancelButton;
	
	public EditAnswerSetDialog(JFrame frame) {
		super(frame, true);
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
	
	}

	public void init() {
		this.setTitle("Edit answer set");
		this.textArea = new JTextArea();
		scrollPane = new JScrollPane(textArea);
		fullFillArea();
		this.setLocation(200,200);
		applyButton = new JButton("Apply");
		cancelButton = new JButton("Cancel");
		initButtonEvents();
		GroupLayout layout = new GroupLayout(this.getContentPane());
		this.getContentPane().setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addComponent(scrollPane,250,350,Short.MAX_VALUE)
				.addGroup(layout.createSequentialGroup()
					.addComponent(applyButton)
					.addComponent(cancelButton)));
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(scrollPane,200,500,Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup()
					.addComponent(applyButton)
					.addComponent(cancelButton)));
		pack();
		setVisible(true);
	}

	private void fullFillArea() {
		File answerSetFile = BlManager.getInstance().getAnswerSet().getRelatedFile();
		FileReader reader = null;
		try {
			reader = new FileReader(answerSetFile);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		BufferedReader bufferedReader = new BufferedReader(reader);
		String line = null;
		try {
			line = bufferedReader.readLine();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		while(line!=null) {
			textArea.append(line +"\n");
			try {
				line = bufferedReader.readLine();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			
		}
	}

	private void initButtonEvents() {
		applyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BufferedWriter write = null;
				File file = null;
				try {
					file = BlManager.getInstance().getAnswerSet().getRelatedFile();
					write = new BufferedWriter(new FileWriter(file));
					write.append(textArea.getText());
					write.flush();
					write.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				MainInterfaceManager.getInstance().closeEditAnswerSetDialog();
				MainInterfaceManager.getInstance().submitChoosenFile(file);
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainInterfaceManager.getInstance().closeEditAnswerSetDialog();
			}
		});
	}
}
