package GUI.mainInterface;

import GUI.mainInterface.specialDialogs.EditAnswerSetDialog;
import java.awt.Dimension;
import java.awt.HeadlessException;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.UIManager;


public class MainFrame extends JFrame {

	private IconToolBar iconToolBar;
	private MenuBar menuBar;
	private GraphicRepresentationPanel graphicRepresentationPanel;
	private DefinitionsPanelAndOptions definitionsPanelAndOptions;
	private JPanel mainPanel;
	private PredicatesPanelAndOptions predicatesPanelAndOptions;
	private int xDim = 600;
	private int yDim = 380;
	private EditAnswerSetDialog editAnswerSetDialog;
	
	public MainFrame(String title) throws HeadlessException {
		super(title);
			try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());    
		} catch (Exception e) {			
		}
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	}

	public void createAndShow() {
		initInterface();
		this.setVisible(true);
		this.pack();
	}

	private void initInterface() {
		mainPanel = new JPanel();
		mainPanel.setPreferredSize(new Dimension(xDim,yDim));
		iconToolBar = new IconToolBar();
		iconToolBar.init();
		menuBar = new MenuBar();
		menuBar.init();
		setJMenuBar(menuBar);	
		mainPanel.add(iconToolBar);
		graphicRepresentationPanel = new GraphicRepresentationPanel();
		graphicRepresentationPanel.init();
		mainPanel.add(graphicRepresentationPanel);
		definitionsPanelAndOptions = new DefinitionsPanelAndOptions();
		definitionsPanelAndOptions.init();
		mainPanel.add(definitionsPanelAndOptions);
		JSeparator toolAndRepresentationSeparator = new JSeparator(SwingConstants.HORIZONTAL);
		predicatesPanelAndOptions = new PredicatesPanelAndOptions();
		predicatesPanelAndOptions.init();
		mainPanel.add(predicatesPanelAndOptions);
		this.getContentPane().add(mainPanel);
		
		
		GroupLayout layout = new GroupLayout(mainPanel);
        mainPanel.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(false);
		layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(iconToolBar)
				.addComponent(toolAndRepresentationSeparator,GroupLayout.Alignment.TRAILING, 0, xDim, Short.MAX_VALUE)
				.addGroup(layout.createSequentialGroup()
					.addGap(0,xDim/3,xDim/3)
					.addComponent(graphicRepresentationPanel))
				.addGroup(layout.createSequentialGroup()
					.addComponent(predicatesPanelAndOptions)
					.addComponent(definitionsPanelAndOptions))	
				
				
        );
		layout.setVerticalGroup(
            layout.createSequentialGroup()            
                .addComponent(iconToolBar)
				.addComponent(toolAndRepresentationSeparator)
				.addComponent(graphicRepresentationPanel)
				.addGroup(layout.createParallelGroup()
					.addComponent(predicatesPanelAndOptions)
					.addComponent(definitionsPanelAndOptions))
        );
		
	}

	public PredicatesPanel getPredicatesPanel() {
		return predicatesPanelAndOptions.getPredicatesPanel();
	}

	public GraphicRepresentationPanel getGraphicRepresentationPanel() {
		return graphicRepresentationPanel;
	}

	public void setEditAnswerSetDialog(EditAnswerSetDialog editAnswerSetDialog) {
		this.editAnswerSetDialog = editAnswerSetDialog;
	}

	public EditAnswerSetDialog getEditAnswerSetDialog() {
		return editAnswerSetDialog;
	}

	public PredicatesPanelAndOptions getPredicatesPanelAndOptions() {
		return predicatesPanelAndOptions;
	}

	public DefinitionsPanelAndOptions getDefinitionsPanelAndOptions() {
		return definitionsPanelAndOptions;
	}
	
	
	
}
