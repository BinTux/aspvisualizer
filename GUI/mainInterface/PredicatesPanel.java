
package GUI.mainInterface;

import mediators.MainInterfaceManager;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import mediators.TableInterfaceManager;


public class PredicatesPanel extends JScrollPane {

	private JTable predicateTable;
	private PredicatesTableModel tableModel;
	public PredicatesPanel() {
		super();
	}
	
	public void init(){
		predicateTable = new JTable();	
		predicateTable.getTableHeader().setReorderingAllowed(false);
		this.setViewportView(predicateTable);		
		predicateTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!e.getValueIsAdjusting()) {
					MainInterfaceManager.getInstance().updateSelectedPredicates(getSelectedRowFirstValues());
				}
			}			
		});
	}
	
	public void addColumns(List<String> labels) {
		tableModel = new PredicatesTableModel(labels.toArray(), 0);
		this.predicateTable.setModel(tableModel);		
	}
	
	public void addRow(List<String> row) {
		this.tableModel.addRow(row.toArray());
	}
	
	public Set<String> getSelectedRowFirstValues() {
		Set<String> rowSelected = new HashSet<>();
		ListSelectionModel selectionModel = predicateTable.getSelectionModel();
		int minIndex = selectionModel.getMinSelectionIndex();
		int maxIndex = selectionModel.getMaxSelectionIndex();
		for (int i = minIndex; i <= maxIndex; i++) {
			if (selectionModel.isSelectedIndex(i)) {
				rowSelected.add ((String)tableModel.getValueAt(i, 0));
			}
		}
		return  rowSelected;
	}

	public void initLabelEvents() {
		predicateTable.getModel().addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent e) {
				int row = e.getFirstRow();
				int column = e.getColumn();
				MainInterfaceManager.getInstance().labelChanged(row,column);
				
			}
		}); 
	}

	public JTable getTable() {
		return predicateTable;
	}
	
	
}
