
package GUI.mainInterface;

import javax.swing.table.DefaultTableModel;


public class PredicatesTableModel extends DefaultTableModel{

	public PredicatesTableModel(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		if(column == 0) {
			return false;
		}			
		if(this.getValueAt(row, column) == null || this.getValueAt(row, column)== "") {
			return false;
		}
		return true;
	}
	
}
