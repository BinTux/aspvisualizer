package GUI.mainInterface;

import mediators.MainInterfaceManager;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;


public class MenuBar extends JMenuBar {

	public MenuBar() {
		super();
	}

	public void init() {
		JMenu fileMenu = new JMenu("File");
		JMenu editMenu = new JMenu("Edit");
		initFileMenu(fileMenu);
		initEditMenu(editMenu);
		this.add(fileMenu);
		this.add(editMenu);
		
	}

	private void initFileMenu(JMenu fileMenu) {
		fileMenu.add(new JMenuItem("New"));
		JMenuItem submitAnswerSet = new JMenuItem("Submit answer set");
		submitAnswerSet.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MainInterfaceManager.getInstance().submitAnswerSetRequest();
			}
		});
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MainInterfaceManager.getInstance().exitRequest();
			}
		});
		fileMenu.add(submitAnswerSet);
		fileMenu.add(new JMenuItem("Save"));
		fileMenu.add(exit);
	}

	private void initEditMenu(JMenu editMenu) {
		
	}
	
}
