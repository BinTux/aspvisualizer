
package GUI.mainInterface;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class DefinitionsPanelAndOptions extends JPanel{
	
	private DefinitionsPanel definitionsPanel;
	private JButton editDefinitionButton;
	private JLabel label;

	public DefinitionsPanelAndOptions() {
		super();
	}
	
	public void init() {
		this.definitionsPanel = new DefinitionsPanel();
		definitionsPanel.init();
		this.add(definitionsPanel);
		label = new JLabel("Graphic definitions");
		this.add(label);
		editDefinitionButton = new JButton("Edit definition...");
		this.add(editDefinitionButton);
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(label)
				.addComponent(definitionsPanel)
				.addGroup(layout.createSequentialGroup()
					.addComponent(editDefinitionButton))
				);
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(label)
				.addComponent(definitionsPanel)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(editDefinitionButton))
				);
	}

	public DefinitionsPanel getDefinitionsPanel() {
		return definitionsPanel;
	}
	
	
	
}
