/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface.specialDialogs;

import bl.aspDomain.Predicate;
import bl.appState.BlManager;
import bl.representationDomain.GridAxis;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.plaf.basic.BasicListUI;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import javax.swing.tree.DefaultMutableTreeNode;
import mediators.GridInterfaceManager;
import sun.org.mozilla.javascript.internal.DefiningClassLoader;

/**
 *
 * @author Andrea
 */
public class DomainAttributeSetupDialog extends JDialog {
	
	private JList selectedDomains;
	private JTree predicatesAndLabels;
	private GridInterfaceManager mediator;
	private JButton addAttributeButton;
	private JButton removeAttributeButton;
	private JButton confirmButton;
	private JButton cancelButton;
	private String currentSelectedPredicate;
	private Map<Predicate,List<Integer>> domainAttributesCopy;
	private GridAxis type;
	
	public DomainAttributeSetupDialog(JDialog owner, GridInterfaceManager mediator, GridAxis axis){
		super(owner,true);
		this.type = axis;
		initSelectedDomainsList();
		this.mediator = mediator;
		buildPredicatesAndLabels();
		initButtons();
		setLayout();
		currentSelectedPredicate = null;
	    deepCopyDomainAttributes();
		this.pack();
		setVisible(true);
	}
	
	private void buildPredicatesAndLabels(){
		DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
		rootNode.setUserObject("Predicates");
		DefaultMutableTreeNode currentNode;
		Set<Predicate> predicates = mediator.getInvolvedPredicates();
		for (Predicate currentPredicate : predicates){
			currentNode = new DefaultMutableTreeNode(currentPredicate.toString());
			List<String> atomsLabel = mediator.getDefinition().getAttributeLabels().get(currentPredicate);
			for (String currentLabel : atomsLabel){
				currentNode.add(new DefaultMutableTreeNode(currentLabel));
			}
			rootNode.add(currentNode);
		}
		predicatesAndLabels = new JTree(rootNode);
		predicatesAndLabels.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)predicatesAndLabels.getLastSelectedPathComponent();
				if (node.isLeaf()){
					//node = (DefaultMutableTreeNode)node.getParent();
					addAttributeButton.setEnabled(true);
					currentSelectedPredicate = (String)((DefaultMutableTreeNode)node.getParent()).getUserObject();
					
				}else{
					addAttributeButton.setEnabled(false);
					currentSelectedPredicate = (String)node.getUserObject();
				}
				changeSelectedDomains(currentSelectedPredicate);

			}
		}); 
		
		JScrollPane scrollPane = new JScrollPane(predicatesAndLabels);
		this.add(scrollPane);
	}
	private void changeSelectedDomains(String predicateSelected){
		Predicate currentPredicate = BlManager.getInstance().getPredicateFromName(predicateSelected);
		List<String> labels = mediator.getDomainPane(type).domainLabelsOfPredicate(predicateSelected);
		DefaultListModel<String> listModel = new DefaultListModel<>();
		for (String currentLabel : labels){
			listModel.addElement(currentLabel);
		}
		removeAttributeButton.setEnabled(false);
		selectedDomains.setModel(listModel);
	}
	
	private void setLayout(){
		GroupLayout layout = new GroupLayout(this.getContentPane());
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		layout.setHorizontalGroup(layout.createParallelGroup()
				.addGroup(layout.createSequentialGroup()
					.addComponent(predicatesAndLabels,100,200,Short.MAX_VALUE)
					.addComponent(addAttributeButton)
					.addGroup(layout.createParallelGroup()
						.addComponent(selectedDomains,50,100,Short.MAX_VALUE)
						.addComponent(removeAttributeButton,GroupLayout.Alignment.CENTER)))
				.addGroup(GroupLayout.Alignment.CENTER,layout.createSequentialGroup()
					.addComponent(confirmButton)
					.addComponent(cancelButton)));
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(predicatesAndLabels,200,300,Short.MAX_VALUE)
					.addComponent(addAttributeButton,GroupLayout.Alignment.CENTER)
					.addGroup(layout.createSequentialGroup()
						.addComponent(selectedDomains,100,200,Short.MAX_VALUE)
						.addComponent(removeAttributeButton)))
				.addGroup(layout.createParallelGroup()
					.addComponent(confirmButton)
					.addComponent(cancelButton)));
		
		this.getContentPane().setLayout(layout);
	}
	
	private void initButtons(){
		addAttributeButton = new JButton(">>");
		addAttributeButton.setActionCommand("add");
		addAttributeButton.addActionListener(new ButtonActionListener());
		addAttributeButton.setEnabled(false);
		
		removeAttributeButton = new JButton("Remove");
		removeAttributeButton.setActionCommand("remove");
		removeAttributeButton.addActionListener(new ButtonActionListener());
		removeAttributeButton.setEnabled(false);
		
		confirmButton = new JButton("Confirm");
		confirmButton.setActionCommand("confirm");
		confirmButton.addActionListener(new ButtonActionListener());
		
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("cancel");
		cancelButton.addActionListener(new ButtonActionListener());
		
		add(addAttributeButton);
		add(removeAttributeButton);
		add(cancelButton);
		add(confirmButton);
	}
	
	private class ButtonActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// add or remove actions
			if (e.getActionCommand().equals(addAttributeButton.getActionCommand()) || e.getActionCommand().equals(removeAttributeButton.getActionCommand())){
				Predicate currentPredicate = BlManager.getInstance().getPredicateFromName(currentSelectedPredicate);
				List<Integer> domains = domainAttributesCopy.get(currentPredicate);

				if (e.getActionCommand().equals(addAttributeButton.getActionCommand())){
					DefaultMutableTreeNode selectedAttribute = (DefaultMutableTreeNode)predicatesAndLabels.getLastSelectedPathComponent();
					int indexOfTheAttributeToAdd = retreiveIndex((String)selectedAttribute.getUserObject());
					domains.add(indexOfTheAttributeToAdd);
					((DefaultListModel)selectedDomains.getModel()).addElement((String)selectedAttribute.getUserObject());

				}else if (e.getActionCommand().equals(removeAttributeButton.getActionCommand())){
					if (domainsAttributeCount() == 1){
						JOptionPane.showMessageDialog(DomainAttributeSetupDialog.this, "At least one attribute must remain for "+ type +" domain indexing.",
								"Cannot delete attribute", JOptionPane.ERROR_MESSAGE);
					}else{
					int indexOfTheSelectedAttribute = retreiveIndex((String)selectedDomains.getSelectedValue());
						domains.remove(indexOfTheSelectedAttribute-1);
					((DefaultListModel)selectedDomains.getModel()).removeElement((String)selectedDomains.getSelectedValue());
					}
					
				}
			}else if (e.getActionCommand().equals(confirmButton.getActionCommand())){
				mediator.getDefinition().setDomains(type, domainAttributesCopy);
				mediator.domainChanged(type);
				DomainAttributeSetupDialog.this.dispose();
			}else if (e.getActionCommand().equals(cancelButton.getActionCommand())){
				DomainAttributeSetupDialog.this.dispose();
			}
		}
		
	}
	
	private void initSelectedDomainsList(){
		selectedDomains = new JList();
		selectedDomains.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!removeAttributeButton.isEnabled()){
					removeAttributeButton.setEnabled(true);
				}
			}
		});
		JScrollPane scrollPane = new JScrollPane(selectedDomains);
		add(scrollPane);
		
	}
	
	private int retreiveIndex (String attributeLabel){
		List<String> labels = mediator.getDefinition().getPredicateLabels(currentSelectedPredicate);
		int i = 1;
		for (String currentString : labels){
			if (currentString.equals(attributeLabel)){
				return i;
			}
			i++;
		}
		//TODO should be thrown an exception
		return i;
	}
	
	private void deepCopyDomainAttributes(){
		domainAttributesCopy = new HashMap<>();
		Map <Predicate,List<Integer>> original = mediator.getDefinition().getAnswerSetDomains(type);
		for (Map.Entry<Predicate,List<Integer>> currentEntry : original.entrySet()){
			domainAttributesCopy.put(currentEntry.getKey(), new ArrayList<Integer>());
			for (Integer currentInteger : currentEntry.getValue()){
				domainAttributesCopy.get(currentEntry.getKey()).add(new Integer(currentInteger.intValue()));
			}
		}
	}
	
	private int domainsAttributeCount (){
		int dim = 0;
		for (Predicate currentPredicate : domainAttributesCopy.keySet()){
			dim+= domainAttributesCopy.get(currentPredicate).size();
		}
		return dim;
	}
	
}
