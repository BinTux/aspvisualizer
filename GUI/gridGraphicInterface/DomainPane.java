/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface;


import GUI.gridGraphicInterface.specialDialogs.DomainAttributeSetupDialog;
import bl.aspDomain.Predicate;
import bl.representationDomain.GridAxis;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;
import listeners.LabelsListener;
import mediators.GridInterfaceManager;

/**
 *
 * @author Andrea
 */
public class DomainPane extends JPanel{
	
	private ButtonGroup domainOrders;
	private JRadioButton occurrenceRadioButton;
	private JRadioButton alphabeticalRadioButton;
	private JRadioButton inverseAlphaRadioButton;
	private JRadioButton numericalRadioButton;
	private JRadioButton inverseNumericalRadioButton;
	
	private JTable attributes;
	private JScrollPane attributesScrollPane;
	private JCheckBox useManuallySpecifiedDomainBox;
	private JButton setManuallySpecifiedDomainButton;
	private JButton addDomainAttributeButton;
	private JButton removeDomainAttributeButton;
	private JButton domainPreviewButton;
	
	private GridInterfaceManager mediator;
	private GridAxis type;
	private LabelsListener labelsListener = new LabelsListener() {

		@Override
		public void labelsChanged(String[] newLabels) {
			changedLabels(newLabels);
		}
	};
	public DomainPane(GridAxis axis,GridInterfaceManager mediator){
		super();
		this.mediator = mediator;
		this.type = axis;
		setBorder(new TitledBorder(axis + " domain"));
		initTable();
		attributesScrollPane = new JScrollPane(attributes);
		useManuallySpecifiedDomainBox = new JCheckBox("use manually set domain");
		setManuallySpecifiedDomainButton = new JButton("set domain");
		addDomainAttributeButton = new JButton("Add");
		addDomainAttributeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new DomainAttributeSetupDialog( DomainPane.this.mediator.getDialog(), DomainPane.this.mediator, DomainPane.this.type);
			}
		});
		removeDomainAttributeButton = new JButton("Remove");
		removeDomainAttributeButton.setEnabled(false);
		domainPreviewButton = new JButton("Preview");
		initButtonGroup();
		addListenerToCheckBox();
		mediator.registerToLabelsListeners(labelsListener);
		setLayout();
	}
	
	private void initButtonGroup(){
		domainOrders = new ButtonGroup();
		numericalRadioButton = new JRadioButton("numerical");
		alphabeticalRadioButton = new JRadioButton("alphabetical");
		inverseAlphaRadioButton = new JRadioButton("inverse alphabetical");
		occurrenceRadioButton = new JRadioButton("occurrence");
		inverseNumericalRadioButton = new JRadioButton("inverse numerical");
		
		domainOrders.add(occurrenceRadioButton);
		domainOrders.add(alphabeticalRadioButton);
		domainOrders.add(inverseAlphaRadioButton);
		domainOrders.add(numericalRadioButton);
		domainOrders.add(inverseNumericalRadioButton);
		domainOrders.setSelected(occurrenceRadioButton.getModel(), true);
		
	}
	
	private void setLayout(){
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(attributesScrollPane,110,110,Short.MAX_VALUE)
					.addGroup(layout.createSequentialGroup()
						.addComponent(addDomainAttributeButton,90,90,90)
						.addGap(5,5,Short.MAX_VALUE)
						.addComponent(removeDomainAttributeButton,90,90,90))
					.addComponent(useManuallySpecifiedDomainBox))
				.addGroup(layout.createParallelGroup()					
						.addComponent(occurrenceRadioButton)
						.addComponent(alphabeticalRadioButton)
						.addComponent(inverseAlphaRadioButton)
						.addComponent(numericalRadioButton)
						.addComponent(inverseNumericalRadioButton)
						.addComponent(domainPreviewButton)
						.addComponent(setManuallySpecifiedDomainButton, GroupLayout.Alignment.LEADING)));
				
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(attributesScrollPane,50,80,Short.MAX_VALUE)				
					.addGroup(layout.createSequentialGroup()
						.addComponent(inverseNumericalRadioButton)
						.addComponent(occurrenceRadioButton)
						.addComponent(alphabeticalRadioButton)
						.addComponent(inverseAlphaRadioButton)
						.addComponent(numericalRadioButton)
						.addComponent(domainPreviewButton)))				
				.addGroup(layout.createParallelGroup()
					.addComponent(addDomainAttributeButton)
					.addComponent(removeDomainAttributeButton))
				.addGroup(layout.createParallelGroup()
					.addComponent(useManuallySpecifiedDomainBox)					
					.addComponent(setManuallySpecifiedDomainButton)));
                layout.linkSize(domainPreviewButton,setManuallySpecifiedDomainButton);
		this.setLayout(layout);
	}

	private void initTable(){
		DomainTableModel tableModel = new DomainTableModel(mediator.getInvolvedPredicates());
		attributes = new JTable(tableModel);
		attributes.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				removeDomainAttributeButton.setEnabled(true);
			}
		});
		attributes.getTableHeader().setReorderingAllowed(false);
	}
	
	private void changedLabels(String [] newLabels){
		attributes.setModel(new DomainTableModel(mediator.getInvolvedPredicates()));
	}
	private void addListenerToCheckBox(){
		useManuallySpecifiedDomainBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addDomainAttributeButton.setEnabled(!addDomainAttributeButton.isEnabled());
				if(!attributes.getSelectionModel().isSelectionEmpty()){
					removeDomainAttributeButton.setEnabled(!removeDomainAttributeButton.isEnabled());
				}
				attributes.setEnabled(!attributes.isEnabled());
				Enumeration<AbstractButton> radioButtons = domainOrders.getElements();
				while(radioButtons.hasMoreElements()){
					AbstractButton currentButton = radioButtons.nextElement();
					currentButton.setEnabled(!currentButton.isEnabled());
				}
				
			}
		});
	}
	
	
	private class DomainTableModel extends AbstractTableModel {
	    String [] labels = new String[] {"Predicate","Domain Attribute"};
		String [][] data;
		
		public DomainTableModel(Set<Predicate> predicates){
			Map<Predicate,List<Integer>> defaultDomains = mediator.getDefinition().getAnswerSetDomains(type);
			int i = 0;
			//TODO controllare size
			data = new String [mediator.getDefinition().getDomainSize(type)][2];
			for (Predicate currentPredicate : predicates){
				List<String> atomsLabel = mediator.getDefinition().getPredicateLabels(currentPredicate);
				List<Integer> currentIndex = defaultDomains.get(currentPredicate);
				for (Integer index : currentIndex ){
					data[i][0]   = currentPredicate.toString();
					data[i++][1] = atomsLabel.get(index-1);
				}
			}
		}

		@Override
		public int getRowCount() {
			return data.length;
		}

		@Override
		public int getColumnCount() {
			return data[0].length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			return data[rowIndex][columnIndex];
		}
		@Override
		public String getColumnName(int col) {
			return labels[col];
		}
		
	}
	
	public List<String> domainLabelsOfPredicate(String predicate){
		List<String> result = new ArrayList<>();
		for (int i = 0; i < attributes.getRowCount(); i++){
			if (attributes.getValueAt(i, 0).equals(predicate)){
				result.add((String)attributes.getValueAt(i, 1));
			}
		}
		return result;
	}
	//TODO unificare con changeLabels
	public void changeDomains(){
		attributes.setModel(new DomainTableModel(mediator.getInvolvedPredicates()));
	}
}
