/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface;

import GUI.sharedComponents.PredicateList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import mediators.GridInterfaceManager;

/**
 *
 * @author Andrea
 */
public class PredicatePane extends JScrollPane implements PredicateList {
	
	private JList<String> predicatesList;
	private DefaultListModel listModel;
    private	GridInterfaceManager mediator;
	
	public PredicatePane(GridInterfaceManager mediator){
		super();
		this.mediator = mediator;
		setBorder(new TitledBorder("Predicate list"));
		listModel = new DefaultListModel();
		initListModel(mediator.getInvolvedPredicatesNames());
		predicatesList = new JList<>(listModel);
		predicatesList.setSelectedIndex(0);
		predicatesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		predicatesList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (predicatesList.getSelectedIndex() != -1){
					PredicatePane.this.mediator.selectionChanged(getCurrentSelection());
				}
			}
		});
		add(predicatesList);
		setViewportView(predicatesList);
	}

	private void initListModel(List<String> predicates) {
		int i = 0;
		for (String currentPredicate : predicates){
			listModel.addElement(currentPredicate);
		}
	}
	
	@Override
	public String getCurrentSelection(){
		return (String)listModel.get(predicatesList.getSelectedIndex());
	}

	@Override
	public ListModel getModel() {
		return listModel;
	}
}
