/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import mediators.GridInterfaceManager;

/**
 *
 * @author Andrea
 */
public class GridDefinitionPanel extends JPanel{
	
	private PredicatePane predicateList;
	private AttributePane attributeTable;
	private IndexingAttributePane indexingAttributePane;
	private VisualDescriptionPane visualDescriptionPane;
	private MainDomainPane domainPane;
	private GridOthersPane othersPane;
	private GridInterfaceManager mediator;
	private JButton submitButton;
	private JButton cancelButton;
	
        /**
         * Empty constructor for mediator.
         */
	public GridDefinitionPanel (){
            super();
        }
	public void initGridDefinitionPanel(GridInterfaceManager mediator){
		this.mediator = mediator;
		predicateList = new PredicatePane(mediator);
		attributeTable = new AttributePane(mediator);
		indexingAttributePane = new IndexingAttributePane(mediator);
		visualDescriptionPane = new VisualDescriptionPane(mediator);
		domainPane = new MainDomainPane(mediator);
		othersPane = new GridOthersPane();
		initConfirmCancelButtons();
		initGroupLayout();
		setVisible(true);
	}
	
	private void initGroupLayout(){
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(layout.createParallelGroup()
				.addGroup(GroupLayout.Alignment.LEADING,layout.createSequentialGroup()
					.addComponent(predicateList,100,180,Short.MAX_VALUE)
					.addGroup(layout.createParallelGroup()
						.addComponent(attributeTable,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE,Short.MAX_VALUE)
						.addComponent(indexingAttributePane,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE,Short.MAX_VALUE)
						.addComponent(visualDescriptionPane,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE,Short.MAX_VALUE)))
				.addComponent(domainPane,GroupLayout.Alignment.LEADING,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE,Short.MAX_VALUE)
				.addComponent(othersPane, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
				.addGroup(GroupLayout.Alignment.CENTER,layout.createSequentialGroup()
					.addGap(20,20,Short.MAX_VALUE)
					.addComponent(submitButton)
					.addGap(20,20,Short.MAX_VALUE)
					.addComponent(cancelButton)
					.addGap(100,100,Short.MAX_VALUE)));
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(predicateList,50,GroupLayout.PREFERRED_SIZE,Short.MAX_VALUE)
					.addGroup(GroupLayout.Alignment.CENTER,layout.createSequentialGroup()
						.addComponent(attributeTable,50,100,Short.MAX_VALUE)
						.addGap(15)
						.addComponent(indexingAttributePane)
						.addGap(15)
						.addComponent(visualDescriptionPane)))
				.addComponent(domainPane,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE,Short.MAX_VALUE)
				.addComponent(othersPane,100,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE)
				.addGroup(layout.createParallelGroup()
					.addComponent(submitButton)
					.addComponent(cancelButton)));
		
		this.setLayout(layout);
	}

	private void initConfirmCancelButtons(){
		submitButton = new JButton("Submit");
		submitButton.setActionCommand("Confirm");
		submitButton.addActionListener(new ConfirmCancelButtonListener());
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		cancelButton.addActionListener(new ConfirmCancelButtonListener());
		
		
	}
	
	public PredicatePane getPredicateList() {
		return predicateList;
	}

	public AttributePane getAttributeTable() {
		return attributeTable;
	}

	public IndexingAttributePane getIndexingAttributePane() {
		return indexingAttributePane;
	}

	public VisualDescriptionPane getVisualDescriptionPane() {
		return visualDescriptionPane;
	}

	public MainDomainPane getDomainPane() {
		return domainPane;
	}

	public GridOthersPane getOthersPane() {
		return othersPane;
	}

	public GridInterfaceManager getMediator() {
		return mediator;
	}
	
	private class ConfirmCancelButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals(submitButton.getActionCommand())){
				mediator.submitGraphicDefinition();
				mediator.disposeDialog();
			}else if(e.getActionCommand().equals(cancelButton.getActionCommand())){
				mediator.disposeDialog();
			}
		
		}
	}
	
}
