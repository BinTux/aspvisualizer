/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Andrea
 */
public class GridOthersPane extends JPanel{
	
	private JCheckBox gridBackgroundCheckBox;
	private JCheckBox attributeOverlappingCheckBox;
	private JCheckBox saveLabelsCheckBox;
	private JTextField imageTextPath;
	private JButton chooseImageButton;
	private JFileChooser imageChooser;
	
	public GridOthersPane (){
		super();
		setBorder(new TitledBorder("Others"));
		gridBackgroundCheckBox = new JCheckBox("set a grid background");
		gridBackgroundCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				imageTextPath.setEnabled(!imageTextPath.isEnabled());
				chooseImageButton.setEnabled(!chooseImageButton.isEnabled());
			}
		});
		attributeOverlappingCheckBox = new JCheckBox("attribute overlapping");
		saveLabelsCheckBox = new JCheckBox("save labels");
		imageTextPath = new JTextField();
		imageTextPath.setEnabled(false);
		chooseImageButton = new JButton("..");
		chooseImageButton.setEnabled(false);
		setLayout();
	}
	
	private void setLayout(){
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);		
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(gridBackgroundCheckBox)
					.addGroup(layout.createSequentialGroup()
						.addComponent(imageTextPath, 100, 100, Short.MAX_VALUE)
						.addComponent(chooseImageButton)))
				.addGap(100, 100, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup()
					.addComponent(attributeOverlappingCheckBox)
					.addComponent(saveLabelsCheckBox))
				.addGap(100, 100, Short.MAX_VALUE));
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(gridBackgroundCheckBox)
					.addComponent(attributeOverlappingCheckBox))					
				.addGroup(layout.createParallelGroup()
					.addComponent(imageTextPath, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addComponent(chooseImageButton)
					.addComponent(saveLabelsCheckBox)));
		this.setLayout(layout);
	}
	
	public boolean willLabelBeOverwritten(){
		return saveLabelsCheckBox.isSelected();
	}
}
