/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface;

import bl.representationDomain.GridAxis;
import com.sun.org.apache.xerces.internal.impl.xpath.XPath;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import mediators.GridInterfaceManager;

/**
 *
 * @author Andrea
 */
public class MainDomainPane extends JPanel{
	
	private DomainPane columnDomainPane;
	private DomainPane rowDomainPane;
	
	public MainDomainPane(GridInterfaceManager mediator){
		rowDomainPane = new DomainPane(GridAxis.ROW,mediator);
		columnDomainPane = new DomainPane(GridAxis.COLUMN,mediator);
		setBorder(new TitledBorder("Domains"));
		setLayout();
	}
	
	private void setLayout(){
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateContainerGaps(true);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addComponent(rowDomainPane)
				.addComponent(columnDomainPane));
		
		layout.setVerticalGroup(layout.createParallelGroup()
				.addComponent(rowDomainPane)
				.addComponent(columnDomainPane));
		
	}
	
	public DomainPane getSpecificDomainPane (GridAxis axis){
		if (axis == GridAxis.ROW){
			return rowDomainPane;
		}
		return columnDomainPane;
	}
	
}
