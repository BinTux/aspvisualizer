/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface;

import listeners.PredicateListener;
import GUI.sharedComponents.AtomsTable;
import bl.aspDomain.Atom;
import bl.aspDomain.Predicate;
import bl.appState.BlManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import mediators.GridInterfaceManager;
import mediators.TableInterfaceManager;

/**
 *
 * @author Andrea
 */
public class AttributePane extends JScrollPane implements AtomsTable {
	
	private JTable table;
	private GridInterfaceManager mediator;
	private PredicateListener predicateListener = new PredicateListener() {

		@Override
		public void predicateChanged(String predicate) {
			initTable(predicate);
		}
	};
	
	public AttributePane(GridInterfaceManager mediator){
		super();
		this.mediator = mediator;
		table = new JTable();
		initTable(mediator.getCurrentSelectedPredicate());
		JTableHeader header = new JTableHeader();
		table.getTableHeader().setReorderingAllowed(false);
		mediator.registerToPredicateListeners(predicateListener);
		this.setViewportView(table);
	}
	
	private void initTable(String predicate){
		Predicate currentPredicate = BlManager.getInstance().getPredicateFromName(predicate);
		List<Atom> atoms = BlManager.getInstance().getAtomsFromPredicate(currentPredicate);
		List<String> labels = mediator.getDefinition().getPredicateLabels(currentPredicate);
		table.setModel(new AtomsTableModel(atoms, labels.toArray(new String[0])));
		initPopup();
	}

	@Override
	public void setModel(DefaultTableModel model) {
		table.setModel(model);
	}

	@Override
	public void setColumnLabels(String[] labels) {
		((AtomsTableModel)table.getModel()).setColumnsIdentifiers(labels);
	}

	
	private class AtomsTableModel extends AbstractTableModel{
		
		private String[][] atoms;
		private String [] labels;
		
		public AtomsTableModel(List<Atom> atoms,String [] labels){
			this.atoms = new String[atoms.size()][labels.length];
			for (int i = 0; i < atoms.size(); i++){
				List<String> currentAttributes = atoms.get(i).getAttributes();
				for (int j = 0; j < currentAttributes.size(); j++){
					this.atoms[i][j] = currentAttributes.get(j);
				}
			}
			this.labels = labels;
		}
		@Override
		public int getRowCount() {
			return atoms.length;
		}

		@Override
		public int getColumnCount() {
			return atoms[0].length;
		}

		@Override
		public String getValueAt(int rowIndex, int columnIndex) {
			return atoms[rowIndex][columnIndex];
		}
		@Override
		public String getColumnName(int col) {
			return labels[col];
		}
		
		public void setColumnsIdentifiers(String [] newLabels){
			labels = newLabels;
			fireTableStructureChanged();
			mediator.labelsChanged(newLabels);
		}
		
	}
	private void initPopup() {
		final JPopupMenu popupMenu= new JPopupMenu();          
		JMenuItem rename= new JMenuItem("Rename labels");
		rename.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mediator.changeLabelsDialogRequest();
			}
		});
		popupMenu.add(rename);
		table.getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				if(mouseEvent.getButton()== MouseEvent.BUTTON3 ) {
					popupMenu.show(table, mouseEvent.getX(), mouseEvent.getY());
				}

			};
		});
	}
		
}
