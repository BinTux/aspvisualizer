/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface;

import bl.appState.BlManager;
import bl.aspDomain.Predicate;
import bl.representationDomain.GridAxis;
import bl.representationDomain.defaultConfigurations.GridDefaultConfiguration;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicComboBoxUI;
import listeners.LabelsListener;
import listeners.PredicateListener;
import mediators.GridInterfaceManager;

/**
 *
 * @author Andrea
 */
public class IndexingAttributePane extends JPanel{
	
	private JLabel rowLabel;
	private JLabel columnLabel;
	private JComboBox rowIndexingAttribute;
	private JComboBox columnIndexingAttribute;
	private LabelsListener labelsListener = new LabelsListener() {

		@Override
		public void labelsChanged(String[] newLabels) {
			changeLabels(newLabels);
		}
	};
	private PredicateListener predicateListener = new PredicateListener() {

	@Override
	public void predicateChanged(String predicate) {
		initComboBoxes(predicate);
	}
	};
        private GridInterfaceManager mediator;
	
	public IndexingAttributePane (GridInterfaceManager mediator){
		super();
        this.mediator = mediator;
		setBorder(new TitledBorder("Indexing attributes"));
		rowLabel = new JLabel("Row indexing attribute:");
		columnLabel = new JLabel("Column indexing attribute:");
		columnIndexingAttribute = new JComboBox();
		rowIndexingAttribute = new JComboBox();
		columnIndexingAttribute.setActionCommand("row");
		rowIndexingAttribute.setActionCommand("column");
		rowIndexingAttribute.addItemListener(new ItemChangedListener());
		columnIndexingAttribute.addItemListener(new ItemChangedListener());
		
		initComboBoxes(mediator.getCurrentSelectedPredicate());
		setLayout();
        mediator.registerToPredicateListeners(predicateListener);
		mediator.registerToLabelsListeners(labelsListener);
	}
	
	private void setLayout(){
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(rowLabel)
					.addComponent(rowIndexingAttribute,30,50,120))
				.addGap(20)
				.addGroup(layout.createParallelGroup()
					.addComponent(columnLabel)
				.addComponent(columnIndexingAttribute,30,50,120)));
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
				.addComponent(rowLabel)
				.addComponent(columnLabel))
				.addGroup(layout.createParallelGroup()
				.addComponent(rowIndexingAttribute,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE)
				.addComponent(columnIndexingAttribute,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE)));
		
		this.setLayout(layout);
	}
        
	private void initComboBoxes(String predicateName){
		Predicate currentPredicate = BlManager.getInstance().getPredicateFromName(predicateName);
		String[] labels = new String[currentPredicate.getAriety()];
		mediator.getDefinition().getAttributeLabels().get(currentPredicate).toArray(labels);
		columnIndexingAttribute.setModel(new DefaultComboBoxModel(labels));
		rowIndexingAttribute.setModel(new DefaultComboBoxModel(labels));
		columnIndexingAttribute.setSelectedIndex(mediator.getDefinition().getIndexingAttributes(GridAxis.COLUMN).get(currentPredicate)-1);
		rowIndexingAttribute.setSelectedIndex(mediator.getDefinition().getIndexingAttributes(GridAxis.ROW).get(currentPredicate)-1);
	}

	private void changeLabels (String [] labels){
		int previousRowSelectedIndex = rowIndexingAttribute.getSelectedIndex();
		int previousColumnSelectedIndex = columnIndexingAttribute.getSelectedIndex();
		columnIndexingAttribute.setModel(new DefaultComboBoxModel(labels));
		rowIndexingAttribute.setModel(new DefaultComboBoxModel(labels));
		columnIndexingAttribute.setSelectedIndex(previousColumnSelectedIndex);
		rowIndexingAttribute.setSelectedIndex(previousRowSelectedIndex);
	}
	
	private class ItemChangedListener implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if ( ((JComboBox)e.getSource()).getActionCommand().equals(IndexingAttributePane.this.rowIndexingAttribute.getActionCommand())){
				//System.out.println(((JComboBox)e.getSource()).getSelectedIndex());
				//TODO chiama due volte??
				mediator.indexingAttributeChanged(GridAxis.ROW,((JComboBox)e.getSource()).getSelectedIndex()+1);
			}else{
				mediator.indexingAttributeChanged(GridAxis.COLUMN,((JComboBox)e.getSource()).getSelectedIndex()+1);
			}
		}	
		
	}
	
}
