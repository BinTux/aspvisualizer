/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface;

import GUI.sharedComponents.GraphicDefinitionDialog;
import GUI.sharedComponents.PredicateList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import mediators.GridInterfaceManager;

/**
 * The main grid customization form.
 * 
 */
public class GridDialog extends GraphicDefinitionDialog{
	
	private GridDefinitionPanel gridPanel;
	
	public GridDialog (JFrame owner,GridDefinitionPanel gridPanel) {
		super(owner);
		this.gridPanel = gridPanel;
		setTitle("Grid Graphic Definition");
		setIconImage(new ImageIcon("icons/grid_graphic_definition/grid.png").getImage());
		add(gridPanel);
	}
	
	public void initDialog (GridInterfaceManager mediator){
		gridPanel.initGridDefinitionPanel(mediator);
		super.init(gridPanel.getPredicateList(), gridPanel.getAttributeTable());
		this.pack();
		this.setVisible(true);
	}
	
	public GridDefinitionPanel getGridPanel(){
		return gridPanel;
	}

	@Override
	public PredicateList getPredicateList() {
		return gridPanel.getPredicateList();
	}
	
}
