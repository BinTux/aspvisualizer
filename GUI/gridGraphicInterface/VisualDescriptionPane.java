/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.gridGraphicInterface;

import listeners.PredicateListener;
import bl.aspDomain.Predicate;
import bl.appState.BlManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import listeners.LabelsListener;
import mediators.GridInterfaceManager;

/**
 *
 * @author Andrea
 */
public class VisualDescriptionPane extends JPanel{
	
	private JCheckBox imageAssociation;
	private JComboBox nameToShowBox;
	private JLabel comboBoxLabel;
	private GridInterfaceManager mediator;
	private LabelsListener labelListener = new LabelsListener() {

		@Override
		public void labelsChanged(String[] newLabels) {
			changelabels(newLabels);
		}
	};
	private PredicateListener listener = new PredicateListener() {

	@Override
	public void predicateChanged(String predicate) {
		initComboBox(predicate);
	}
	};
	
	public VisualDescriptionPane (GridInterfaceManager mediator){
		super();
        this.mediator = mediator;
		setBorder(new TitledBorder("Visual description"));
		nameToShowBox = new JComboBox();
		nameToShowBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				VisualDescriptionPane.this.mediator.visualDescriptionChanged(((JComboBox)e.getSource()).getSelectedIndex());
			}
		});
		initComboBox(mediator.getCurrentSelectedPredicate());
		imageAssociation = new JCheckBox("Image association");
		comboBoxLabel = new JLabel("Description field:");
		setLayout();
        mediator.registerToPredicateListeners(listener);
		mediator.registerToLabelsListeners(labelListener);
	}
	
	private void setLayout(){
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(comboBoxLabel)
					.addComponent(nameToShowBox,30,50,120))
				.addGap(25)
				.addComponent(imageAssociation));
		
		layout.setVerticalGroup(layout.createParallelGroup()
				.addGroup(layout.createSequentialGroup()
					.addComponent(comboBoxLabel)
					.addComponent(nameToShowBox,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE))
					.addComponent(imageAssociation,GroupLayout.Alignment.TRAILING));
		this.setLayout(layout);
	}
        
	private void initComboBox(String predicateName){
		Predicate currentPredicate = BlManager.getInstance().getPredicateFromName(predicateName);
		String[] visualizationLabels = new String[currentPredicate.getAriety()+1];
		visualizationLabels[0] = currentPredicate.getName();
		List<String> attributeLabels = mediator.getDefinition().getPredicateLabels(currentPredicate);
		int i = 1;
		for (String currentLabel : attributeLabels){
			visualizationLabels[i++] =currentLabel;
		}
		nameToShowBox.setModel(new DefaultComboBoxModel(visualizationLabels));
		nameToShowBox.setSelectedIndex(mediator.getVisualDescriptor());
	}
	
	private void changelabels(String [] newLabels){
		String [] shiftedLabels = new String[newLabels.length+1];
		//predicate name
		shiftedLabels [0] = (String)nameToShowBox.getItemAt(0);
		for (int i = 1; i <= newLabels.length; i++){
			shiftedLabels[i] = newLabels[i-1];
		}
		int previousIndex = nameToShowBox.getSelectedIndex();
		nameToShowBox.setModel(new DefaultComboBoxModel(shiftedLabels));
		nameToShowBox.setSelectedIndex(previousIndex);
	}
		
}